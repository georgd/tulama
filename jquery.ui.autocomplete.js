/* 
  TU lAma - let Alma be more adroit

  extends jquery.ui.autocomplete.html
 
  Copyright (C) 2019 Leo Zachl, Technische Universität Wien, Bibliothek

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

(function( $ ) {

  var proto = $.ui.autocomplete.prototype;

  $.extend( proto, {

    _renderMenu: function(ul, items){
      var self = this;

      if (items.length > 0 && items[0].counts != undefined){
        console.log(items[0]);
        if(!!items[0].link)
          $("<li class='autocomplete_title'><b><a target='_blank' href='"+items[0].link + "'>"+items[0].counts.limit+" Titel von ("+items[0].counts.count+"/"+items[0].counts.titles+")</a></b></li>").appendTo(ul);
        else
          $("<li class='autocomplete_title'><b>"+items[0].counts.limit+" Titel von ("+items[0].counts.count+"/"+items[0].counts.titles+")</b></li>").appendTo(ul);
      }
      $.each(items, function (index, item) {
                         
        if (item.sub != undefined){
          $.each(item.sub, function (index, sub) {
            sub.class = 'tuwsub';
            self._renderItemData(ul, sub);
          });
        }
        self._renderItemData(ul, item);
      });
    }
  });

})( jQuery );
