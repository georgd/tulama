/* 
  TU lAma - let Alma be more adroit

  popup menu
 
  Copyright (C) 2019 Leo Zachl, Technische Universität Wien, Bibliothek

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

function listenForClicks() {
  document.addEventListener("click", (e) => {
    var id = e.target.id;
    if (id != ''){
      var xhr = new XMLHttpRequest();
      console.log('clicked ' + id);
      xhr.open("GET", config.bmsUrl + id + ".js?nocache_" + (new Date()).getTime());
      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
          var scriptToExecute = '(function(){' + xhr.responseText + '})();';
          console.log('script: ' + scriptToExecute);
          if (id.startsWith('unlock') && !!config.unlockPriKey)
            scriptToExecute = scriptToExecute.replace('__UNLOCK_PRIVATE_KEY__',config.unlockPriKey);
	  if (id.startsWith('lock') && !!config.lockPriKey)
            scriptToExecute = scriptToExecute.replace('__LOCK_PRIVATE_KEY__',config.lockPriKey);
          var gettingCurrent = browser.tabs.query({currentWindow: true, active: true, url: "*://*.alma.exlibrisgroup.com/*"});
          gettingCurrent.then(function (tabs) {
            console.log(tabs);
            for (let tab of tabs) {
              console.log('script: ' + id);
              browser.tabs.executeScript(tab.id, {
                code: "(function (scriptBase64) { var scriptElement = document.createElement(\"script\"); scriptElement.innerHTML = atob(scriptBase64); scriptElement.type = \"text/javascript\"; document.body.appendChild(scriptElement); }(\"" + btoa(scriptToExecute) + "\"));"
              },function(){window.close();});
            }
          });
	}
      };
      xhr.send(null);
    }
  });
}

browser.storage.local.get().then(function(result){
  config = result;
  if (!config.bmsUrl && !!config.tuwSeen || config.bmsUrl == "https://almagw.ub.tuwien.ac.at/ubintern/bmls/"){
    config.bmsUrl = "https://almagw.ub.tuwien.ac.at/tulama/bmls/";
    browser.storage.local.set({bmsUrl: config.bmsUrl});
  }
  if (!result.disabledBmlsCats){ result.disabledBmlsCats = [ 'onetime','hss','stb','erw' ]; }
  if (!!config.bmsUrl){
    if (config.bmsUrl.substr(-1) != '/')
      var permissions = {
        origins: [config.bmsUrl + '/']
      };
    else
      var permissions = {
        origins: [config.bmsUrl]
      };

    browser.permissions.contains(permissions).then((permitted) => {
      if (permitted){
        var xhr = new XMLHttpRequest();
        console.log('load bookmarklets menue from ' + config.bmsUrl);
        xhr.open("GET", config.bmsUrl + "menu.html?nocache_"+(new Date()).getTime());
        xhr.onreadystatechange = function () {
          if (xhr.readyState === 4 && xhr.status === 200) {
            var bmls = $(xhr.responseText);
            if (!!result.disabledBmlsCats) result.disabledBmlsCats.forEach(function(cat){bmls.filter('.'+cat).hide();});
            $('#popup-content').empty().append(bmls);
            listenForClicks();
            var cats = [];
            var menu = $(xhr.responseText).filter('div.bookmarklet');
            menu.each(function(){
              var classes = this.className.split(' ');
              classes.shift();
              classes.shift();
              classes.forEach(function(cat){
                if (cats.find(function(dog){ return (dog.cat == cat);}) == undefined){
                  cats.push({cat: cat, tit: $('div.'+cat+' strong').text()});
                }
              });
            });
            browser.storage.local.set({bmlsCats: cats});
          }
        };
        xhr.send(null);
        if (!!config.locationIPURL && (!config.locMap || config.locationIPURL == 'https://almagw.ub.tuwien.ac.at/tulama/locMap.json')){
          if (config.locationIPURL == 'https://almagw.ub.tuwien.ac.at/tulama/locMap.json'){
            config.locationIPURL = 'https://almagw.ub.tuwien.ac.at/tulama/locMapName.json';
            browser.storage.local.set({locationIPURL:config.locationIPURL});
          }
          var xhrlm = new XMLHttpRequest();
          console.log('loaded IP to Location map from ' + config.locationIPURL);
          xhrlm.responseType = 'json';
          xhrlm.open("GET", config.locationIPURL + "?nocache_"+(new Date()).getTime());
          xhrlm.onreadystatechange = function () {
            if (xhrlm.readyState === 4 && xhr.status === 200) {
              console.log(xhrlm.response);
              browser.storage.local.set({locMap: xhrlm.response});
            }
          };
          xhrlm.send(null);
        } else if (!config.locationIPURL && !!config.locMap){
          browser.storage.local.set({locMap: false});
        }
      } else {
        var bmsUrl = config.bmsUrl;
        var urlRe = /^(https?):\/\/([^\/]*)(\/.*)?$/;
        var match = urlRe.exec(bmsUrl);
        $('#popup-content').empty().append($('<h3>Neue Berechtigung notwendig</h3><p style="padding: 10px;">Bitte erlauben Sie das Nachladen der Bookmarklets von<br/><b>'+match[1] + '://' + match[2] +'/</b><br/><button>Berechtigungen verwalten</button></p>'));
        $('#popup-content button').click(function(){
          browser.tabs.create({
            url: browser.runtime.getURL("permissions.html")
          });
        });
      }
    });
  }
});

$('div#version').text(' lAma v' + browser.runtime.getManifest().version);
