function addOrigin(origin,checked){
  var node = document.createElement("LI");
  var nodeText = document.createTextNode(" "+origin);
  var checkBox = document.createElement("input");
  checkBox.type = 'checkbox';
  checkBox.checked = checked;
  checkBox.dataset.origin=origin;
  if (origin == "*://*.alma.exlibrisgroup.com/*")
    checkBox.disabled = true;
  else
    checkBox.addEventListener('change', processChange);
  node.appendChild(checkBox);
  node.appendChild(nodeText);
  document.getElementById("origins").appendChild(node);
}

function updatePermissions() {
  browser.permissions.getAll()
  .then((permissions) => {
    document.getElementById('permissions').innerText = permissions.permissions.join(', ');
    document.getElementById("origins").innerHTML = '';
    for (let origin of permissions.origins) {
      addOrigin(origin,true);
    }
    browser.storage.local.get(['apiURL','bmsUrl','loansUrl','tuwsysUrl','locationIPURL']).then((urls) => {
      var re = /^(https?):\/\/([^\/]*)(\/.*)?$/;
      var match;
      var origins = new Array();
      for (let url in urls) {
        if (match = re.exec(urls[url])){
          origins.push(match[1]+'://'+match[2]+'/*');
        }
      }
      origins = Array.from(new Set(origins));
      for (let origin in origins) {
        browser.permissions.contains({origins:[origins[origin]]}).then((result) => {
          if (!result) addOrigin(origins[origin],false);
        });
      }
    });
  });
}

async function processChange(event) {
  var origin = event.target.dataset.origin;
  var permission = {origins: [origin]};
  let result = document.getElementById('result');

  try {
    if (event.target.checked) {
      await browser.permissions.request(permission)
      .then((response) => {
        result.className = 'bg-success';
        result.textContent = (lang=='de')?'Host origin hinzugefügt':'Host origin added';
      })
      .catch((err) => {
        // Catch the case where the permission cannot be granted.
        result.className = 'bg-warning';
        result.textContent = err.message;
      });
    } else {
      browser.permissions.remove(permission)
      .then((response) => {
        result.className = 'bg-success';
        result.textContent = (lang=='de')?'Host origin gelöscht':'Host origin deleted';
      });
    }
  } catch(err) {
    // Catch the case where the permission is completely wrong.
    result.className = 'bg-danger';
    result.textContent = err.message;
  }
  result.style.display = 'block';
  updatePermissions();
  event.preventDefault();
}

var lang = browser.i18n.getUILanguage().substr(0,2);
document.getElementById('body').classList.add(lang);
updatePermissions();
