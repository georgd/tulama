# tu-lAma

** tu-lAma let Alma be more adroit **

## General function

*   Choose location by IP address
*   Expand list of search items (IDs, MMS-IDs, ISBN): with Ctrl-Alt-Y
*   Syntax highlighting for Marc21 (like in AlEn by Michael Birkner)
*   High contrast (black font)
*   Warning on duplicates or records marked for deletion (970)
*   Dynamic loading of bookmarklets (macros)
*   Sorting of "Item List" by description Ctrl-Alt-D
*   Move messages to bottom of window

## MD-Editor

*   Jump to first empty field or scroll to end of record
*   "Expand from predefined template" with Ctrl-Alt-T
*   Replace empty 852_8_c on save
*   Keep cataloging level

## Item Editor

*    Apply template Ctrl-Alt-T, change template values Shift-Ctrl-Alt-T
*    Change item policy Ctrl-Alt-C, change value Shift-Ctrl-Alt-C
*    Change prozes-typeCtrl-Alt-B, change value Shift-Ctrl-Alt-B
