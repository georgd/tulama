/* 
  TU lAma - let Alma be more adroit

  settings script
 
  Copyright (C) 2019 Leo Zachl, Technische Universität Wien, Bibliothek

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

var locMap = false;

function saveOptions(e) {
  e.preventDefault();

  // check origins permissions
  // and open permissions tab if needed, requesting new permissions is not working in about:addons
  var origins = new Array();
  for (let urlId of ['bmsUrl','loansUrl','tuwsysUrl','apiURL','locationIPURL']){
    var origin = document.querySelector('#'+urlId).value;
    if (origin != ''){
      if (origin.substr(-1) != '/') origin = origin + '/';
      origins.push(origin);
    }
  }
  if (origins.length > 0){
    browser.permissions.contains({origins: origins}).then((result) => {
      if (!result){
        browser.tabs.create({
          url: browser.runtime.getURL("permissions.html")
        });
      }
    });
  }

  browser.storage.local.set({
    loansUrl: document.querySelector('#loansUrl').value,
    bmsUrl: document.querySelector('#bmsUrl').value,
    tuwsysUrl: document.querySelector('#tuwsysUrl').value,
    apiURL: document.querySelector('#apiURL').value,
    apiKey: document.querySelector('#apiKey').value,
    processSlip: $('#processSlip').prop("checked"),
    lockPriKey: document.querySelector('#lockPriKey').value,
    unlockPriKey: document.querySelector('#unlockPriKey').value,
    selectLocation: !$('#selectLocation').prop("checked"),
    disabledBmlsCats: $('#bmsCats').val(),
    keepCatLevel: !$('#keepCatLevel').prop("checked"),
    checkBug00576546: !$('#checkBug00576546').prop("checked"),
    goto1stEmpty: !$('#goto1stEmpty').prop("checked"),
    addLinks: !$('#addLinks').prop("checked"),
    addPortfolio: !!$('#addPortfolio').prop("checked"),
    templateName: document.querySelector('#templateName').value,
    add852c: document.querySelector('#add852c').value,
    locationIPURL: document.querySelector('#locationIPURL').value,
    locMap: !$('#locationIPURL').val()?false:locMap,
    colorScheme: $('#colorScheme').val(),
    overwriteAlmaColors: $('#overwriteAlmaColors').prop("checked"),
    highContrast: $('#highContrast').prop("checked"),
    debugLevel: $('#debugLevel').val()
  });
}
function restoreOptions() {
  function setCurrentChoice(result) {
    if (!!result.tuwSeen) $('body').removeClass('tuw');
    if (!result.bmsUrl && !!result.tuwSeen || result.bmsUrl == 'https://almagw.ub.tuwien.ac.at/ubintern/bmls/') result.bmsUrl = 'https://almagw.ub.tuwien.ac.at/tulama/bmls/';
    document.querySelector('#loansUrl').value = result.loansUrl || '';
    document.querySelector('#bmsUrl').value = result.bmsUrl || '';
    document.querySelector('#tuwsysUrl').value = result.tuwsysUrl || '';
    document.querySelector('#apiURL').value = result.apiURL || '';
    document.querySelector('#apiKey').value = result.apiKey || '';
    $('#processSlip').prop("checked", !!result.processSlip).trigger('change');
    document.querySelector('#lockPriKey').value = result.lockPriKey || '';
    document.querySelector('#unlockPriKey').value = result.unlockPriKey || '';
    // document.querySelector('#').value = result. || '';
    document.querySelector('#templateName').value = result.templateName || '0_SE-3-SYS';
    document.querySelector('#add852c').value = result.add852c || 'UNASSIGNED';
    $('#selectLocation').prop("checked",!result.selectLocation).trigger('change');
    var cats = "<select multiple='multiple' id='bmsCats' name='bmsCats' size='1'>\n";
    if (!!result.bmlsCats){
      result.bmlsCats.forEach(function(cat){ cats += "<option id='" + cat.cat + "' title='"+cat.tit+"' value='" + cat.cat + "'>"+ cat.cat + "</option>\n"; });
    }
    var msel = $(cats + "</select>");
    $('#bmsCatsTd').empty().append(msel);
    if (!result.disabledBmlsCats){ result.disabledBmlsCats = [ 'onetime','hss','stb','erw' ]; }
    if (!!result.bmlsCats){
      result.disabledBmlsCats.forEach(function(cat){$('option#'+cat).prop('selected',true).trigger('change');});
    }
    if (!!result.colorScheme){
      $('#colorScheme option#'+result.colorScheme).prop('selected',true).trigger('change');
    }
    if (!!result.debugLevel){
      $('#debugLevel option#'+result.debugLevel).prop('selected',true).trigger('change');
    }
    $('#overwriteAlmaColors').prop("checked",!!result.overwriteAlmaColors).trigger('change');
    $('#highContrast').prop("checked",!!result.highContrast).trigger('change');
    $('#keepCatLevel').prop("checked",!result.keepCatLevel).trigger('change');
    $('#checkBug00576546').prop("checked",!result.checkBug00576546).trigger('change');
    $('#goto1stEmpty').prop("checked",!result.goto1stEmpty).trigger('change');
    $('#addLinks').prop("checked",!result.addLinks).trigger('change');
    $('#addPortfolio').prop("checked",!!result.addPortfolio).trigger('change');
    document.querySelector('#locationIPURL').value = result.locationIPURL || '';
    locMap = (!!result.locMap)?locMap:false;
  }
  function onError(error) {
    console.log('Error: ${error}')
  }
  var getting = browser.storage.local.get();
  getting.then(setCurrentChoice, onError)
}
document.addEventListener('DOMContentLoaded', restoreOptions);
document.querySelector('form').addEventListener('submit', saveOptions);
$('[data-i18n]').each(function () {
  var dataI18n = $(this).data('i18n');
  var innerHtml = $(this).html();
  $(this).html(browser.i18n.getMessage(dataI18n))
});
