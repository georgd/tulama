/* 
  TU lAma - let Alma be more adroit

  background script
 
  Copyright (C) 2019 Leo Zachl, Technische Universität Wien, Bibliothek

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

function handleMessage(request, sender, sendResponse) {
  if (request.action == 'openPopup'){
    var creating = browser.windows.create({
      url: request.popupURL,
      type: 'popup',
      height: 600,
      width: 800
    });
  } else {
    var gettingCurrent = browser.tabs.query({currentWindow: true, active: true});
    gettingCurrent.then(function (tabs) {
      for (let tab of tabs) {
        browser.tabs.sendMessage(
          tab.id,
          request
        );
        console.log(tab.id); console.log(request);
      }
    });
  }
}

browser.runtime.onMessage.addListener(handleMessage);

browser.webNavigation.onHistoryStateUpdated.addListener(function (details) {
  var gettingCurrent = browser.tabs.query({currentWindow: true, active: true});
  gettingCurrent.then(function (tabs) {
    request = { action: 'historyStateUpdated' };
    for (let tab of tabs) {
      browser.tabs.sendMessage(
        tab.id,
        request
      );
      console.log(tab.id); console.log(request);
    }
  });
});

browser.storage.local.get().then(function(result){
  if (result.selectLocation == undefined)
    browser.storage.local.set({ selectLocation: true });
  if (result.goto1stEmpty == undefined)
    browser.storage.local.set({ goto1stEmpty: true });
  if (result.addLinks == undefined)
    browser.storage.local.set({ addLinks: true });
  if (result.highContrast == undefined)
    browser.storage.local.set({ highContrast: true });
});
