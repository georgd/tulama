/* 
  TU lAma - let Alma be more adroit

  content script
 
  Copyright (C) 2019 Leo Zachl, Technische Universität Wien, Bibliothek

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

var toastobj;
var deltoast;
var isTuw = false;
var dialog;

function debug(msg,level = 1){
  return new Promise(function(resolv, reject){
    browser.storage.local.get().then(function(result){
      if (!!result.debugLevel && result.debugLevel >= level)
        resolv(msg);
    });
  });
}

function toast(msg, col,sek){
  // message am unteren rand ~10 sec lang, col als hintergrund
  // einmal drauf clicken: nachricht beliebt stehen
  // ein zweitesmal drauf clicken: nachricht wird entfernt.
  if (toastobj != undefined) toastobj.remove();
  if (deltoast != undefined) clearTimeout(deltoast);
  toastobj = $("<div style='position: fixed; width: 100%; bottom: 0; display: none; background: " + col + "; padding: 5px; z-index: 1000; '>" + msg + "</div>");
  $('body').append(toastobj);
  toastobj.fadeIn(1000).delay(sek*1000).fadeOut(1000);
  deltoast = setTimeout(function(){
    toastobj.remove();
  }, sek*1000);
  toastobj.click(function(){
    clearTimeout(deltoast);
    toastobj.stop(true).show().click(function(){toastobj.remove();});
  });
}

function search_items(){
  var title = $('#MarcEditorView\\.title:contains(Bestand), #MarcEditorView\\.title:contains(Holding)');
  if (title.length == 0){
    title = $('#MarcEditorView\\.title:contains(Bibliografisch), #MarcEditorView\\.title:contains(Bibliographic)');
  }
  if (title.length == 1){
    var idre = /\((\d*)\)/;
    var match = idre.exec(title.text());
    if (match && match[1] != ''){
      debug(match[1],2).then(function(m){console.log(m)});
      browser.runtime.sendMessage({action: "search", what: "ITEM", id: match[1]});
    }
  } else {
    return false;
  }
}

function setSimpleSearchKey(keyStr){
  var label = findSimpleSearchKeyLabel(keyStr);
  if(keyStr && label){
    setSimpleSearchKeyAndLabel(keyStr, label);
  }
}

function setSimpleSearchKeyAndLabel(searchKey, searchLabel){
  $('#simpleSearchKey').val(searchKey);
  $('#simpleSearchKeyDisplay').html(searchLabel);
  $('#simpleSearchKey').change();
  $('#simpleSearchKeyDisplay').parent('button').attr("title",searchLabel);
}

function findSimpleSearchKeyLabel(keyStr){
  var label;
  $('.simpleSearchKeys').each(function(){
     var searchKey = $(this).data("search-key");
     var searchLabel = $(this).data("search-label");
     if(searchKey === keyStr) label= searchLabel;
     return;
  });
  return label;
}

var retries = 0;
function addButtons(){
  browser.storage.local.get().then(function(result){
    if (!result.addLinks){
      if ($('table.MenuIconBarUxp').length == 0 && retries < 10){
        retries++;
        setTimeout(addButtons,1000);
      } else {
        var g2i= $('#MenuIconBar\\.Go2Items');
        if (g2i.length == 1){
          var button = g2i.closest('td');
        } else {
          var button = $('<td style="vertical-align: top;" align="left"><div tabindex="0" class="gwt-PushButton new-Design gwt-PushButton-up" role="button" style="width: auto; height: auto; padding-left: 16px; padding-right: 16px;" title="zu den Exemplaren des Titels" aria-pressed="false"><input tabindex="-1" role="presentation" style="opacity: 0; height: 1px; width: 1px; z-index: -1; overflow: hidden; position: absolute;" type="text"><div class="html-face"><span id="MenuIconBar.Go2Items" class="icon-alma-ex-alma-book-phy font20 uxfBlueDarkHover uxfBlue"></span></div></div></td>');
          $('table.MenuIconBarUxp table.gwt-MenuBar-horizontal table tbody tr').append(button);
          button.click(search_items);
        }
      }
    }
  });
}

function addRecordViewLinks(){
  browser.storage.local.get().then(function(result){
    if (!result.addLinks){
      debug('add links',2).then(function(m){console.log(m)});
      var backButton = $('#PAGE_BUTTONS_cbuttonback').parent();
      if (backButton.length > 0){
        var isHolding = ($('#pageBeanRegistryType').text() == 'marc21_holding');
        var mms_id = $('#pageBeanmmsId').text();
        if ($('#PAGE_BUTTONS_cbuttonitems').length == 0){
          var itemsButton = $('<div class="pull-right marLeft10 "> <button type="submit" class="btn btn-secondary jsBlockScreen  jsToolTipDelayed" id="PAGE_BUTTONS_cbuttonitems" title="' + browser.i18n.getMessage('physicalItems') + '" value="' + browser.i18n.getMessage('physicalItems') + '" name="page.buttons.operation">' + browser.i18n.getMessage('physicalItems') + '</button></div>');
          backButton.before(itemsButton);
          itemsButton.on("click",function(){
            browser.runtime.sendMessage({
              action: 'search',
              what: 'ITEM',
              id: mms_id
            });
            return false;
          });
        }
        if (isHolding){
          if ($('#PAGE_BUTTONS_cbuttonbibmms').length == 0){
            var bibButton = $('<div class="pull-right marLeft10 "> <button type="submit" class="btn btn-secondary jsBlockScreen  jsToolTipDelayed" id="PAGE_BUTTONS_cbuttonbibmms" title="' + browser.i18n.getMessage('bib_mms') + '" value="' + browser.i18n.getMessage('bib_mms') + '" name="page.buttons.operation">' + browser.i18n.getMessage('bib_mms') + '</button></div>');
            backButton.before(bibButton);
            bibButton.on("click",function(){
              browser.runtime.sendMessage({
                action: 'search',
                what: 'BIB_MMS',
                id: mms_id
              });
              return false;
            });
          }
        } else {
          if ($('#PAGE_BUTTONS_cbuttonholdings').length == 0){
            var holButton = $('<div class="pull-right marLeft10 "> <button type="submit" class="btn btn-secondary jsBlockScreen  jsToolTipDelayed" id="PAGE_BUTTONS_cbuttonholdings" title="' + browser.i18n.getMessage('holding') + '" value="' + browser.i18n.getMessage('holding') + '" name="page.buttons.operation">' + browser.i18n.getMessage('holding') + '</button></div>');
            backButton.before(holButton);
            holButton.on("click",function(){
              browser.runtime.sendMessage({
                action: 'search',
                what: 'IEP',
                id: mms_id
              });
              return false;
            });
          }
          if (!!result.addPortfolio && $('#PAGE_BUTTONS_cbuttonAddPf').length == 0){
            var addPfButton = $('<div class="pull-right marLeft10 "> <button type="submit" class="btn btn-secondary jsBlockScreen  jsToolTipDelayed" id="PAGE_BUTTONS_cbuttonAddPf" title="' + browser.i18n.getMessage('addPf') + '" value="' + browser.i18n.getMessage('addPf') + '" name="page.buttons.operation">' + browser.i18n.getMessage('addPf') + '</button></div>');
            backButton.before(addPfButton);
            addPfButton.on("click",function(){
              var f856 = $("span[id$='_COL_tag']:contains(856)").closest('tr').find("[id$='_COL_value']:contains(Resolving-System)");
              if (f856.length == 0)
                f856 = $("span[id$='_COL_tag']:contains(856)").closest('tr').find("[id$='_COL_value']:contains(Volltext)");
              var link = f856.find('a').get(0).href;
              if (f856.text()[0] != '4' || !link)
                link = '';
              // console.log(f856);
              window.location.href = '/rep/action/pageAction.do?xmlFileName=ecreation.eresource_creation.xml&pageViewMode=Edit&operation=LOAD&pageBean.mmsId='+ $('#pageBeanmmsId').text() +'&pageBean.portfolioCreationType=USE_EXISTING&RenewBean=true&pageBean.currentUrl=xmlFileName%3Decreation.eresource_creation.xml%26pageViewMode%3DEdit%26operation%3DLOAD%26pageBean.mmsId%3D' + $('#pageBeanmmsId').text() + '%26pageBean.portfolioCreationType%3DUSE_EXISTING%26RenewBean%3Dtrue%26pageBean.currentUrl%3D&pageBean.electronicMaterialType=BOOK&pageBean.url=' + encodeURI(link);
              return false;
            });
          }
        }
      }
    }
  });
}

var curCatLevel;

function recordLoaded(){
  browser.storage.local.get().then(function(result){
    if (!result.keepCatLevel){
      debug('keepCatLevel',2).then(function(m){console.log(m)});
      var curLevel = $('div.gwt-HTML:contains(Aktuelle Katalogisierungs-Ebene), div.gwt-HTML:contains(Current cataloging level)');
      if (curLevel.length > 0){
        debug(curLevel,2).then(function(m){console.log(m)});
        debug(curLevel.text(),2).then(function(m){console.log(m)});
        var re = /(Aktuelle Katalogisierungs-Ebene|Current cataloging level) +\[(\d*)\]/;
        var match = re.exec(curLevel.text());
        debug(match,2).then(function(m){console.log(m)});
        if (match[2] != undefined){
          curCatLevel = match[2];
          debug('current level: '+curCatLevel,2).then(function(m){console.log(m)});
          if (curLevel.closest('tr').find("select.gwt-ListBox option[value='"+curCatLevel+"']").length < 1) curCatLevel = '20';
          curLevel.closest('tr').find("select.gwt-ListBox option[value='"+curCatLevel+"']").prop('selected',true).trigger("change");
        } else {
          debug(match,2).then(function(m){console.log(m)});
        }
      }
    }
  });
}

function gotoFirstEmpty(){
  browser.storage.local.get().then(function(result){
    if (!result.goto1stEmpty && $('#FieldTagBox\\.tag\\.001').length > 0){
      var firstEmpty = $('input#FieldTagBox\\.tag\\.');
      if (firstEmpty.length == 0){
        firstEmpty = $('div[id^="MarcEditorPresenter\\.textArea"] textarea').filter(function(){
          if (this.value.trim() == '$$a' || this.value.trim() == ''){
            if (this.parentNode.parentNode.id.substr(0,31) != 'MarcEditorPresenter.textArea.00')
              this.value = '$$a ';
            return true;
          } else {
            return false;
          }    
        });
      }
      debug(firstEmpty,2).then(function(m){console.log(m)});
      if (firstEmpty.length > 0){
        firstEmpty.eq(0).click().focus();
        firstEmpty.get(0).scrollIntoView(true);
      }
    }
  });
}

var ce = document.createEvent('HTMLEvents');
ce.initEvent('keydown', true, true);
ce.altKey = false;
ce.ctrlKey = true;
ce.code=69;
ce.keyCode=69;
ce.which=69;

var click = document.createEvent('HTMLEvents');
click.initEvent('click', true, true);

var change = document.createEvent('HTMLEvents');
change.initEvent('keyup', false, true);

function load_template(templateName){
  // debug(document).then(function(m){console.log(m)});
  if (!!templateName && $('#FieldTagBox\\.tag\\.001').length > 0){
    var observer = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {
        if (mutation.removedNodes.length >= 1 && $(mutation.removedNodes).find('#MdEditor\\.PopupMsg\\.LOADING').length >= 1){
          $("div.gwt-DialogBox select.gwt-ListBox option:contains(" + templateName + ")").prop('selected',true).trigger('change');
          // debug($("div.gwt-DialogBox select.gwt-ListBox")).then(function(m){console.log(m)});
          $("div.gwt-DialogBox button:contains('Ok')").get(0).dispatchEvent(click);
        }
      });
    });
    observer.observe($('body').get(0),{childList:true,subtree:false});
    obstimer = setTimeout(function(){
      debug('main observer timed out!').then(function(m){console.log(m)});
      observer.disconnect();
    },30000);
    var scriptElement = document.createElement("script");
    scriptElement.type = "text/javascript";
    scriptElement.innerHTML = `var ce = document.createEvent('HTMLEvents');
      ce.initEvent('keydown', true, true);
      ce.altKey = false;
      ce.ctrlKey = true;
      ce.code=69;
      ce.keyCode=69;
      ce.which=69;
      document.dispatchEvent(ce);`;
    document.body.appendChild(scriptElement);
  }
}

var konksuche = '';

function keyscan( event ) {

  function isbn_suche(){
    var isbns = new Array;
    var isbn020 = $('div#MarcEditorPresenter\\.textArea\\.020 textarea');
    var isbn7x6 = $('div#MarcEditorPresenter\\.textArea\\.776 textarea, div#MarcEditorPresenter\\.textArea\\.786 textarea');
    debug(isbn020,2).then(function(m){console.log(m)});
    debug(isbn7x6,2).then(function(m){console.log(m)});
    isbn020.each(function(i,ta){
      var isbn = ta.value.match(/\$\$[az] *([0-9X\-]{10,}) *(\$\$.*)?$/);
      if (isbn){
        isbns.push(isbn[1].replace('-',''));
      }
      debug(isbn,2).then(function(m){console.log(m)});
      debug(ta.value,2).then(function(m){console.log(m)});
    });
    isbn7x6.each(function(i,ta){
      var isbn = ta.value.match(/\$\$z *([0-9X\-]{10,}) *(\$\$.*)?$/);
      if (isbn){
        isbns.push(isbn[1].replace('-',''));
      }
      debug(isbn,2).then(function(m){console.log(m)});
      debug(ta.value,2).then(function(m){console.log(m)});
    });
    if (isbns.length > 0)
      act.autocomplete('search',isbns.join(','));
    else {
      act.autocomplete('search','dasistkeineisbnnummer');
      browser.runtime.sendMessage({
        action: "toast",
        message: "keine ISBNs zur Konkordanzsuche vorhanden, suche nur nach Titel ",
        color: '#ffff8f',
        duration: 20
      });
    }
  }

  debug( event, 3).then(function(m){console.log(m)});
  browser.storage.local.get().then(function(result){
    let strgAlt = (event.altKey || event.metaKey) && event.ctrlKey;
    if (isTuw && !!result.tuwsysUrl && (event.target.parentNode != undefined && event.target.parentNode.parentNode.id == 'MarcEditorPresenter.textArea.983' || konksuche != '')){
      var act = $(event.target );
      var aci = act.autocomplete( "instance" );
      if (aci == undefined)
        var acwv = undefined;
      else
        var acwv = act.autocomplete('widget').is(':visible');
      switch( event.keyCode ) {
        case $.ui.keyCode.LEFT:
          // debug( "left" ).then(function(m){console.log(m)});
          break;
        case $.ui.keyCode.RIGHT:
          // debug( "right" ).then(function(m){console.log(m)});
          break;
        case $.ui.keyCode.UP:
          if (acwv){
            // debug( "up" ).then(function(m){console.log(m)});
            event.stopPropagation();
            event.preventDefault();
            if (event.type == "keydown") aci._keyEvent('previous', event);
            return false;
          }
          break;
        case $.ui.keyCode.DOWN:
          if (acwv){
            // debug( "down" ).then(function(m){console.log(m)});
            event.stopImmediatePropagation();
            event.preventDefault();
            if (event.type == "keydown") aci._keyEvent('next', event);
            return false;
          }
          break;
        case $.ui.keyCode.ENTER:
        case $.ui.keyCode.TAB:
          aci.menu.active && (event.type == "keydown", aci.menu.select(event), event.stopPropagation(), event.preventDefault());
          break;
        case $.ui.keyCode.ESCAPE:
          aci.menu.element.is(':visible') && (aci.isMultiLine || aci._value(aci.term), aci.close(event), event.stopPropagation(), event.preventDefault());
          break;
        case 68: // Ctrl-Alt-D delete
          if (strgAlt){
            event.stopPropagation();
            event.preventDefault();
            if (event.type == "keydown"){
              act.val('$$a ');
              act.trigger('change');
              act.autocomplete('search',act.val());
            }
          }
          break;
        case 71: // Ctrl-Alt-G Geographikum
          if (strgAlt){
            event.stopPropagation();
            event.preventDefault();
            if (event.type == "keydown"){
              var loc = act.val().match(/(.*?)\$\$g *(.*?) *(\$\$.*)?$/);
              if (!loc){
                act.val(act.val().trim() + ' $$g ');
              } else {
                if (loc[3] == undefined)
                  act.val(loc[1] + '$$g ');
                else
                  act.val(loc[1] + loc[3] + ' $$g ');
              }
              act.trigger('change');
              act.autocomplete('search',act.val());
            }
          }
          break;
        case 76: // Ctrl-Alt-L Sprachcode
          if (strgAlt){
            event.stopPropagation();
            event.preventDefault();
            if (event.type == "keydown"){
              var lang = act.val().match(/(.*?)\$\$s *(.*?) *(\$\$.*)?$/);
              // debug(lang).then(function(m){console.log(m)});
              // debug(act.val().substr(4,7)).then(function(m){console.log(m)});
              if (!lang){
                act.val(act.val().trim() + ' $$s ');
              } else {
                if (lang[3] == undefined)
                  if (act.val().substr(4,7) == 'ALL:001')
                    act.val(lang[1] + ' $$s ' + lang[2] + ' $$s ');
                  else
                    act.val(lang[1] + '$$s ');
                else
                  if (act.val().substr(4,7) == 'ALL:001')
                    act.val(lang[1] + lang[3] + ' $$s ' + lang[2] + ' $$s ');
                  else
                    act.val(lang[1] + lang[3] + ' $$s ');
              }
              act.trigger('change');
              act.autocomplete('search',act.val());
            }
          }
          break;
        case 77: // Ctrl-Alt-M Ortskürzel -> µ darum mit Shift oder:
        case 192: // Ctrl-Alt-Ö Ortskürzel unter Linux
          if (strgAlt){
            event.stopPropagation();
            event.preventDefault();
            if (event.type == "keydown"){
              var lang = act.val().match(/(.*?)\$\$o *(.*?) *(\$\$.*)?$/);
              if (!lang){
                act.val(act.val().trim() + ' $$o ');
              } else {
                if (lang[3] == undefined)
                  act.val(lang[1] + '$$o ');
                else
                  act.val(lang[1] + lang[3] + ' $$o ');
              }
              act.trigger('change');
              act.autocomplete('search',act.val());
            }
          }
          break;
        case 78: // Ctrl-Alt-N Namenskürzel
          if (strgAlt){
            event.stopPropagation();
            event.preventDefault();
            if (event.type == "keydown"){
              var pers = act.val().match(/(.*?)\$\$n *(.*?) *(\$\$.*)?$/);
              if (!pers){
                act.val(act.val().trim() + ' $$n ');
              } else {
                if (pers[3] == undefined)
                  act.val(pers[1] + '$$n ');
                else
                  act.val(pers[1] + pers[3].trim() + ' $$n ');
              }
              act.trigger('change');
              act.autocomplete('search',act.val());
            }
          }
          break;
        case 80: // Ctrl-Alt-P Summenzeile neu berechnen
          if (strgAlt){
            event.stopPropagation();
            event.preventDefault();
            if (event.type == "keydown"){
              var lastEmpty;
              var tuSysHgs = new Array;
              $("#MarcEditorPresenter\\.textArea\\.983 textarea").each(function(i,e){
                var regs;
                debug(':'+e.value.trim()+':',4).then(function(m){console.log(m)});
                if (e.value.trim() == '$$a' || e.value.trim() == '' || !!e.value.match(/^\$\$a( [A-Z]{3}:)* *$/)){
                  lastEmpty = e;
                } else {
                  if (regs = e.value.match(/\$\$a ([A-Z]{3}):[A-Z0-9]{3}/)){
                    debug(regs,4).then(function(m){console.log(m)});
                    tuSysHgs.push(regs[1]+':');
                  }
                }
              });
              if (lastEmpty != undefined){
                $(lastEmpty).parent().parent().get(0).dispatchEvent(click);
                $(lastEmpty).focus().click().val('$$a ' + Array.from(new Set(tuSysHgs)).sort().join(' ')).get(0).dispatchEvent(change);
              }
            }
          }
          break;
        case 81: // Ctrl-Alt-Q (Quer-)Verweis auf andere Hauptgruppen
          if (strgAlt){
            event.stopPropagation();
            event.preventDefault();
            if (event.type == "keydown"){
              var pers = act.val().match(/(.*?)\$\$t *(.*?) *(\$\$.*)?$/);
              if (!pers){
                act.val(act.val().trim() + ' $$t ');
              } else {
                if (pers[3] == undefined)
                  act.val(pers[1] + '$$t ');
                else
                  act.val(pers[1] + pers[3].trim() + ' $$t ');
              }
              act.trigger('change');
              act.autocomplete('search',act.val());
            }
          }
          break;
        case 73: // isbn/titel konkordanzsuche
          if (strgAlt){
            konksuche = event.key;
            isbn_suche();
          }
          break;
        case 75:
          // konkordanzsuche
          if (strgAlt){
            konksuche = event.key;
            act.autocomplete('search',act.val());
          }
          break;
      }
    } else if (event.type == "keydown" && event.key == 't' && strgAlt){
      load_template(result.templateName);
    } else if (strgAlt && isTuw && !!result.tuwsysUrl && event.type == "keyup" && (event.keyCode == 75 || event.keyCode == 73 || event.keyCode == 66)){
      konksuche = event.key;
      debug(konksuche,2).then(function(m){console.log(m)});
      act = $('div#MarcEditorPresenter\\.textArea\\.983 textarea').filter(function(){
        return (this.value.trim() == '' || this.value.trim() == '$$a')
      });
      // debug(act).then(function(m){console.log(m)});
      if (act.length > 0){
        act = act.first();
        if (konksuche == 'i' || konksuche == 'I'){
          isbn_suche();
        } else {
          var selectedTextArea = document.activeElement;
          var selection = selectedTextArea.value.substring(selectedTextArea.selectionStart, selectedTextArea.selectionEnd);
          if (selection.length == 0){
            selection = document.getSelection().toString();
          }
          debug(selection,2).then(function(m){console.log(m)});
          if (selection.length > 0)
            act.autocomplete('search',selection);
          else {
            browser.runtime.sendMessage({
              action: "toast",
              message: "bitte einen Begriff zur Konkordanzsuche selektieren, oder im 983er eingeben!",
              color: '#ffff8f',
              duration: 10
            });
            konksuche = '';
          }
        } 
      } else {
        browser.runtime.sendMessage({
          action: "toast",
          message: "bitte vorher 0_SE-3-SYS Template laden (Strg-Alt-T), bzw. ein paar leere 983er anlegen!",
          color: '#ffff8f',
          duration: 10
        });
        konksuche = '';
      }
    }
  });
}

function tuwsys(){

  window.addEventListener("keyup",    keyscan, true);
  window.addEventListener("keydown",  keyscan, true);
  window.addEventListener("keypress", keyscan, true);

  browser.storage.local.get().then(function(result){
    if (isTuw && !!result.tuwsysUrl && result.tuwsysUrl != ''){
      var selected=false;
      var item = false;

      function copy_tuwsys(tuwsys){
        // debug(tuwsys).then(function(m){console.log(m)});
        var e983 = $('div#MarcEditorPresenter\\.textArea\\.983 textarea').filter(function(){
          return (this.value.trim() == '' || this.value.trim() == '$$a')
        });
        if (e983.length < tuwsys.length){
          browser.runtime.sendMessage({
            action: "toast",
            message: "zu wenig leere 983er Felder (" + e983.length + " < " + tuwsys.length + ")!",
            color: '#ffff8f',
            duration: 20
          });
        } else {
          tuwsys.forEach(function(ts,i){
            e983[i].value = ts.value;
            e983[i].dispatchEvent(change);
            $(e983[i]).blur().focus();
          });
        }
      }

      $("#MarcEditorPresenter\\.textArea\\.983 textarea").filter(function(){
        return !(this.value.includes('AT-OBV') || this.value.includes('$$z'));
      }).not(':last').autocomplete({
        source: function (request, response) {
          debug(request.term.substr(-4).trim(),2).then(function(m){console.log(m)});
          if (request.term.substr(-4).trim() == '$$n'){
            // Personen und Körperschaften aus der Verschlagwortung
            var pbs = new Array();
            $("#MarcEditorPresenter\\.textArea\\.689 textarea, #MarcEditorPresenter\\.textArea\\.600 textarea, #MarcEditorPresenter\\.textArea\\.610 textarea").filter(function(){
              return this.parentNode.parentNode.id == 'MarcEditorPresenter.textArea.610'
                  || this.parentNode.parentNode.id == 'MarcEditorPresenter.textArea.600'
                  || !!this.value.match(/\$\$D [bp]/);
            }).each(function(){
              var gnd = this.value.match(/\$\$a *(.*?) *(\$\$|$)/);
              var tus = request.term.trim().match(/\$\$a *(...:...)( .*?) *(\$\$.*|$)/);
              debug(tus,2).then(function(m){console.log(m)});
              pbs.push({
                'value': '$$a ' + tus[1] + ' ' + gnd[1].substr(0,3).toUpperCase() + ' ' + tus[3] + ' ' + gnd[1],
                'label': '<b>' + this.parentNode.parentNode.id.substr(-3) + '</b> ' + $('<span/>').text(this.value.substr(4).replace(/\$\$/g,'|')).html()
              });
            });
            // doppelte einträge löschen
            response(pbs.filter(function (value, index, self){ 
              var fi = self.findIndex(function(val){
                return val.value === value.value;
              });
              return index === fi;
            }));
          } else if (request.term.substr(-4).trim() == '$$o'){
            // Geographikum
            var geos = new Array();
            var tus = request.term.trim().match(/\$\$a *(.*?) (.{3})(:.{3})?( (.{3})?(:.{3})? *)(\$\$.*)$/);
            debug(tus,2).then(function(m){console.log(m)});
            if (tus[1] == ''){
              tus[1] = tus[2] + tus[3];
              tus[2] = '---';
            }
            $("#MarcEditorPresenter\\.textArea\\.689 textarea, #MarcEditorPresenter\\.textArea\\.651 textarea, #MarcEditorPresenter\\.textArea\\.662 textarea").filter(function(){
              return this.parentNode.parentNode.id == 'MarcEditorPresenter.textArea.651'
                  || this.parentNode.parentNode.id == 'MarcEditorPresenter.textArea.662'
                  || !!this.value.match(/\$\$D g/);
            }).each(function(){
              var gnd = this.value.match(/\$\$a *(.*?)( \$\$.*)?$/);
              var gndnr = gnd[2].match(/\$\$0 *\(DE-588\)(.*?)( \$\$|$)/);
              debug(gnd,2).then(function(m){console.log(m)});
              geos.push({
                'value': '$$a ' + tus[1] + ' ' + tus[2] + ':' + gnd[1].substr(0,3).toUpperCase() + tus[4] + tus[7].trim() + ' ' + gnd[1],
                'label': '<b>' + this.parentNode.parentNode.id.substr(-3) + '</b> ' + $('<span/>').text(this.value.substr(4).replace(/\$\$/g,'|')).html(),
                'gndnr': !!gndnr?gndnr[1]:undefined
              });
            });
            // doppelte einträge löschen
            response(geos.filter(function (value, index, self){   
              return index === self.findIndex(function(val){
                return val.value === value.value;
              });
            }));
          } else {
            // debug(request.term).then(function(m){console.log(m)});
            // debug(this).then(function(m){console.log(m)});
            jQuery.post(result.tuwsysUrl, {
              QUERY: request.term,
              TIT: $("#MarcEditorPresenter\\.textArea\\.245 textarea").val(),
              KONK: konksuche,
              MMSID: $("#MarcEditorPresenter\\.textArea\\.001 textarea").val(),
              CURPOS: this.element.get(0).selectionStart
            },
            function (data) {
              // debug(data).then(function(m){console.log(m)});
              if (konksuche != '' && data.length == 0){
                browser.runtime.sendMessage({
                  action: "toast",
                  message: konksuche + " suche nach »" + request.term + "« hat nichts ergeben",
                  color: '#ffff8f',
                  duration: 10
                });
                konksuche = '';
              }
              if (data.length > 0 && data[0].tuwsys != undefined){
                var ndata = new Array;
                var bk = '';
                var sws = '';
                data.forEach(function(item){
                  if (!!item.bk)
                    bk = '<br/>' + item.bk.replace('\n','<br/>');
                  else
                    bk = '';
                  if (!!item.sws)
                    sws = item.sws.replace('\n','<br/>');
                  else
                    sws = '';
                  ndata.push({label: '<b>'+item.ac+'</b><p class="sub">'+ (item.title+' / '+item.autor).replace(/</g,'&lt;').replace(/>/g,'&gt;') + '<br/>' + sws + bk + '</p> ', value: item.tusyss, sub: item.tuwsys});
                  if (!!data[0].limit) ndata[0].counts = {limit: data[0].limit, count: data[0].count, titles: data[0].titles };
                  if (!!data[0].link) ndata[0].link = data[0].link;
                });
                response(ndata);
              } else
                response(data);
            });
          }
        },
        select: function( event, ui ) {
          debug(ui,4).then(function(m){console.log(m)});
          debug(event,4).then(function(m){console.log(m)});
          item = ui.item
          if (ui.item.value != undefined){
            selected = ui.item.value.match(/\$\$a *(.*?) *(\$\$.*|$)/);
            debug(selected,4).then(function(m){console.log(m)});
          }
        },
        close:  function( event, ui ) {
          if (!!item && item.sub != undefined){
            if (this.value.substr(0,3) != '$$a')
              this.value = '$$a ';
            copy_tuwsys(item.sub);
          } else if (!!item && item.gndnr != undefined && this.value.substr(12,3) == '---'){
            var that = this;
            jQuery.post(result.tuwsysUrl, {
              QUERY: item.gndnr,
              KONK: 'G'
            },
            function(data){
              var cur = that.value.match(/\$\$a *(.*?) *(\$\$g .*?)? *(\$\$o .*)?( *\$\$[^dego].*?)?$/);
              if (cur && data[0] != undefined){
                if (cur[4] == undefined) cur[4] = '';
                if (data[0].code.length == 3){
                  if (cur[1].substr(0,3) == 'ALL') data[0].code += ':---';
                  that.value = "$$a " + cur[1].substr(0,8) + data[0].code + cur[1].substr(15) + " $$g " + data[0].g + cur[4];
                } else
                  that.value = "$$a " + cur[1].substr(0,8) + data[0].code + cur[1].substr(15) + " $$g " + data[0].g + " $$o " + data[0].o + cur[4];
                that.dispatchEvent(change);
                $(that).trigger("change");
              } else {
                debug(that,2).then(function(m){console.log(m)});
                debug(data,2).then(function(m){console.log(m)});
              }
            });
          } else {
            this.dispatchEvent(change);
            $(this).trigger("change");
            if (!!selected && (selected[1].length < 8 && (selected[1][3] == ':' || selected[1].trim().length < 4) || selected[0].substr(-4).trim().substr(0,2) == '$$')){
              $(this).autocomplete('search',selected[0]);
            }
          }
          item = false;
          selected=false;
          konksuche = '';
        },
        open: function(){
          debug('ac opened',2).then(function(m){console.log(m)});
        },
        classes: { "ui-autocomplete": "tulama-tuwsys" },
        html: true,
        position: { my: "left top", at: "right bottom", collision: "fit" }
      });

      // automatische isbn/titel konkordanzsuche bei mindestens 8 leeren 983ern
      var e983 = $('div#MarcEditorPresenter\\.textArea\\.983 textarea').filter(function(){
          return (this.value.trim() == '' || this.value.trim() == '$$a')
      });
      if (e983.length == 8){
        var cai = document.createEvent('HTMLEvents');
        cai.initEvent('keyup', true, true);
        cai.altKey = true;
        cai.ctrlKey = true;
        cai.key = 'i';
        cai.code=73;
        cai.keyCode=73;
        cai.which=73;
        e983.get(0).dispatchEvent(cai);
      }
      debug(e983,3).then(function(m){console.log(m)});
    }
  });
}

var last_mmsid;

function habe_fertig(){
  last_mmsid = false;
  debug("habe fertig").then(function(m){console.log(m)});
  if (!!document.getElementById('tu_lAma_blocker')) return;
  addMDEColors();
  gotoFirstEmpty();
  tuwsys();
  checkbug00576546();
}

function catlevel(){

  var shelfLoc = '';

  function check852c(e){
    var change = document.createEvent('HTMLEvents');
    change.initEvent('keyup', false, true);
    change.code = 37;
    change.keyCode = 37;
    change.which = 37;
    var f852_8 = $("#MarcEditorPresenter\\.textArea\\.852 textarea");
    var re = /^(.*)(\$\$c\s+)(.*?)(\$\$.*|$)/;
    var match = re.exec(f852_8.val());
    if (!!match && match[3] == ''){
      f852_8.val(match[1].trim() + ' $$c ' + shelfLoc + ' ' + match[4]);
      f852_8.click().focus().get(0).dispatchEvent(change);
      f852_8.blur();
    }
    e.target.onblur(e);
  }
  
  recordLoaded(); // beim direkten öffnen ist der record hier schon geladen
  var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
      debug(mutation,4).then(function(m){console.log(m)});
      if (mutation.addedNodes.length >= 1){
        if ($(mutation.addedNodes[0]).is('div.html-face:has(i#MenuIconBar\\.deleteRecordImage)')){
          recordLoaded();
        }
        if ($(mutation.addedNodes[0]).is('div.html-face:has(i#MenuIconBar\\.addItemToHoldingImage)')
         || $(mutation.addedNodes[0]).is('div.html-face:has(i#MenuIconBar\\.saveRecordImage)')){
          browser.storage.local.get().then(function(result){
            if (isTuw || !!result.add852c){
              shelfLoc = (!!result.add852c)?result.add852c:'UNASSIGNED';
              mutation.target.onclick = check852c;
            }
          });
        }
      }
    });
  });
  // das sollte die menueleiste sein, bei jedem neuen record werden am ende zumindest teile neu geladen
  debug($('table.sectionContainer').closest('tr').prev().get(0),3).then(function(m){console.log(m)});
  observer.observe($('table.sectionContainer').closest('tr').prev().get(0),{childList:true,subtree:true});
  // manchmal ist der save button von anfang an da
  browser.storage.local.get().then(function(result){
    if (isTuw || !!result.add852c){
      shelfLoc = (!!result.add852c)?result.add852c:'UNASSIGNED';
      $('table.sectionContainer').closest('tr').prev().find('div.html-face:has(i#MenuIconBar\\.saveRecordImage),div.html-face:has(i#MenuIconBar\\.addItemToHoldingImage)')
        .each(function(i,e){
          e.parentNode.onclick = check852c;
        });
    }
  });

  // beim öffnen, änderungen von tags oder indikatoren wird alles zweimal neu aufgebaut tag für tag
  // beim ersten mal merken wir uns das letzte tag (inkl anzahl)
  // beim zweiten durchlauf wissen wir dann beim letzten tag dass wir fertig sind...  
  var tag;
  var cur_mms_id;
  var highest_tag
  var last_tag;
  var nr_last_tag;
  var nr_highest_tag;
  var timer;
  var robserver = new MutationObserver(function(mutations) {
    // console.log(document.getElementById('tu_lAma_blocker'));
    mutations.forEach(function(mutation) {
      // debug(mutation.addedNodes).then(function(m){console.log(m)});
      if (mutation.addedNodes.length >= 1 && $(mutation.addedNodes[0]).is('div[id^="MarcEditorPresenter\\.textArea"]')){
        tag = mutation.addedNodes[0].id.substr(-3);
	if (tag == '001'){
          clearTimeout(timer);
          timer = setTimeout(habe_fertig,500);
          cur_mms_id = $(mutation.addedNodes[0]).find('textarea').val();
          if (cur_mms_id != last_mmsid){
            last_mmsid = cur_mms_id;
            highest_tag = undefined;
            last_tag = '001';
	  } else {
            highest_tag = last_tag;
            nr_highest_tag = nr_last_tag;
          }
	} else if (tag != 'LDR'){
          clearTimeout(timer);
          if (last_tag != tag)
            nr_last_tag = 1;
          else
            nr_last_tag++;
	  last_tag = tag;
	  if (tag == highest_tag){
	    if (nr_highest_tag == 1){
	      habe_fertig();
	    } else {
              nr_highest_tag--;
              timer = setTimeout(habe_fertig,500);
            }
	  } else {
            timer = setTimeout(habe_fertig,500);
          }
	}
        debug(last_mmsid + ' ' + tag + ' ' + cur_mms_id +  ' ' + last_tag +  ' ' + nr_highest_tag + ':' + highest_tag, 3).then(function(m){console.log(m)});
      }
    });
  });
  robserver.observe($('table.sectionContainer').closest('tbody').get(0),{childList:true,subtree:true});
}

function checkbug00576546(){
  // MD_Editor wird geladen ein Datensatz ist schon geöffnet und er ist in einem Set
  // frage ob bei allen records in dem set der Ursprünglich Datensatz geladen werden soll -> lokale felder wieder da
  // sollte also nur kommen wenn man ein set zum katalogisieren neu geöffnet hat
  browser.storage.local.get().then(function(result){
    if (isTuw && !result.checkBug00576546){
      if ($('#FieldTagBox\\.tag\\.001').length > 0){
        var mmsid = $('div#MarcEditorPresenter\\.textArea\\.001 textarea').val();
        var locfs = $('td.localFieldIndication input').map(function() { return this.id.substr(-3); }).get().join(' ');
        debug(mmsid + ': ' + locfs,2).then(function(m){console.log(m)});
        $.post("https://almagw.ub.tuwien.ac.at/tulama/lfs.php",{MMSID: mmsid}, function(data){
          if (data.local_fields == undefined){
            browser.runtime.sendMessage({
              action: "toast",
              message: locfs + ' aktuell geladenen lokale Felder. Keine ältere Version zum Vergleichen gefunden!',
              color: '#ffff8f',
              duration: 20
            });
          } else if (data.local_fields == locfs){
            if (locfs != ''){
              browser.runtime.sendMessage({
                action: "toast", 
                message: 'Lokale Felder i.o.: ' + locfs,
                color: '#8fff8f',
                duration: 5
              });
            } else {
              browser.runtime.sendMessage({
                action: "toast", 
                message: 'Keine lokalen Felder, schaut aber OK aus.',
                color: '#8fff8f',
                duration: 5
              });
            }
          } else if (locfs == ''){
            browser.runtime.sendMessage({
              action: "toast", 
              message: '<b>Achtung keine lokalen Felder! Ursprünglichen Datensatz neuladen.</b><br/>Letzte bekannte Version vom ' + data.file_mod + ': ' + data.local_fields,
              color: '#ff8f8f',
              duration: 20
            });
          } else if (data.local_fields == ''){
            browser.runtime.sendMessage({
              action: "toast",
              message: locfs + ' Lokale Felder aktuell.<br/>' + 'Noch keine lokalen Felder in der Version vom ' + data.file_mod,
              color: '#8fff8f',
              duration: 5
            });
          } else {
            browser.runtime.sendMessage({
              action: "toast", 
              message: locfs + ' Unterschiedliche lokale Felder! Aktuell.<br/>'+ data.local_fields +' Letzte bekannte Version vom ' + data.file_mod,
              color: '#ffff8f',
              duration: 20
            });
          }
        },"json").fail(function(jqXHR, textStatus, errorThrown) {
          debug(textStatus,2).then(function(m){console.log(m)});
        });
      }
    }
  });
}

function sort_items(by){ // Description
  $("#pageBeansortSectionPropertiesroutine_hiddenSelect option[value='"+by+"']").prop('selected',true).trigger('change');
  setTimeout(function(){
    $('#widgetId_Left_sortroutine').removeClass('open');
    $('#TABLE_DATA_list').find("span[id$='_COL_arrivalDate'][title!='-']").get(0).scrollIntoView(false);
    window.scrollBy(0,200);
  },1000);
}

function protype(pt){
  browser.storage.local.get().then(function(result){
    if (!!result.processType || isTuw){
      if (!!result.processType) pt = result.processType.trim();
      if ($('#PAGE_BUTTONS_cresource_editorpitem_generalsave_and_edit').length > 0){
        var obstimer;
        var observer = new MutationObserver(function(mutations) {
          mutations.forEach(function(mutation) {
            if (mutation.attributeName = "class" && $(mutation.target).hasClass("hide")){
              clearTimeout(obstimer);
              observer.disconnect();
              protype(pt);
            }
          });
        });
        observer.observe($('#loadingBlocker').get(0),{childList:true,subtree:true,attributes:true});
        obstimer = setTimeout(function(){
          debug('main observer timed out!').then(function(m){console.log(m)});
          observer.disconnect();
        },30000);
        $('#PAGE_BUTTONS_cresource_editorpitem_generalsave_and_edit').click();
      } else {
        debug('change process type to ' + pt,2).then(function(m){console.log(m)});
        var ptype = $("#pageBeanselectedProcessType_hiddenSelect option[value='"+pt+"']");
        ptype.prop('selected',true);
        $("#pageBeanselectedProcessType_hiddenSelect").get(0).onchange();
      }
    }
  });
}

function proTypeSettings(){
  if (dialog != undefined) dialog.remove();
  dialog = $(`
    <style>
      #itemTemplateSettings br { clear: both; }
      #itemTemplateSettings select, #itemTemplateSettings input { margin-top: 8px; }
      #itemTemplateSettings button { margin-left: 100px; }
      #itemTemplateSettings { position: fixed; top: 200px; display: none; width: 100%; z-index: 10000; }
      #itemTemplateSettings span.de,#itemTemplateSettings span.en  { display: none; }
      html[lang='de'] #itemTemplateSettings span.de { display: inline; }
      html[lang='en'] #itemTemplateSettings span.en { display: inline; }
    </style>
    <div id='itemTemplateSettings'>
      <div style='width: 900px; margin: auto; background: white; padding: 5px; border: solid #666 1px; border-radius: 4px;'>
        <br/>
        <button id='close_settings' style='margin-left: 150px;'>
          <span class='de'>Abbrechen</span>
          <span class='en'>Cancle</span>
        </button>
        <button id='save_settings'>
          <span class='de'>Speichern</span>
          <span class='en'>Save</span>
        </button>
        <button id='apply_settings'>
          <span class='de'>Speichern und Anwenden</span>
          <span class='en'>Save and Apply</span>
        </button>
      </div>
    </div>`);
  dialog.find('div')
    .prepend($("<br/>"))
    .prepend($("#pageBeanselectedProcessType_hiddenSelect").clone().attr('id','tu_lAma_itemTemplProcType').off().removeAttr('onchange').removeAttr('disabled').show().removeClass('hide'))
    .prepend($("[for='pageBeanselectedProcessType']").clone().attr('for','tu_lAma_itemTemplProcType'));
  dialog.find('div').prepend($("<h3 style='text-align: center;'><span class='de'>Prozesstyp Einstellungen</span><span class='en'>Process Type Settings</span></h3>"));

  $('body').append(dialog);
  dialog.show().find('button#close_settings').click(function(){
    dialog.stop(true).remove();
  });
  dialog.find('button#save_settings').click(function(){
    browser.storage.local.set({
      processType: dialog.find('#tu_lAma_itemTemplProcType').val()
    }).then(function(){
        dialog.stop(true).remove();
    });
  });
  dialog.find('button#apply_settings').click(function(){
    browser.storage.local.set({
      processType: dialog.find('#tu_lAma_itemTemplProcType').val()
    }).then(function(){
      dialog.stop(true).remove();
      protype(dialog.find('#tu_lAma_itemTemplProcType').val());
    });
  });

  browser.storage.local.get().then(function(result){
    if (!!result.processType) dialog.find('#tu_lAma_itemTemplProcType').val(result.processType);
  });
}

function set_item_policy(pol){
  browser.storage.local.get().then(function(result){
    var sel = false;
    if (!!result.itemPolicy)
      sel = $("#pageBeanitemMddnxphysicalItemTableitemPolicy_hiddenSelect option[value='"+result.itemPolicy+"']");
    else if (isTuw)
      sel = $("#pageBeanitemMddnxphysicalItemTableitemPolicy_hiddenSelect option[value='"+pol+"']");
    if (!!sel && sel.length > 0){
      $("#pageBeanitemMddnxphysicalItemTableitemPolicy").val(sel.text());
      sel.prop('selected',true).trigger('change');
    }
  });
}

function itemPolicySettings(){
  if (dialog != undefined) dialog.remove();
  dialog = $(`
    <style>
      #itemTemplateSettings br { clear: both; }
      #itemTemplateSettings select, #itemTemplateSettings input { margin-top: 8px; }
      #itemTemplateSettings button { margin-left: 100px; }
      #itemTemplateSettings { position: fixed; top: 200px; display: none; width: 100%; z-index: 10000; }
      #itemTemplateSettings span.de,#itemTemplateSettings span.en  { display: none; }
      html[lang='de'] #itemTemplateSettings span.de { display: inline; }
      html[lang='en'] #itemTemplateSettings span.en { display: inline; }
    </style>
    <div id='itemTemplateSettings'>
      <div style='width: 900px; margin: auto; background: white; padding: 5px; border: solid #666 1px; border-radius: 4px;'>
        <br/>
        <button id='close_settings' style='margin-left: 150px;'>
          <span class='de'>Abbrechen</span>
          <span class='en'>Cancle</span>
        </button>
        <button id='save_settings'>
          <span class='de'>Speichern</span>
          <span class='en'>Save</span>
        </button>
        <button id='apply_settings'>
          <span class='de'>Speichern und Anwenden</span>
          <span class='en'>Save and Apply</span>
        </button>
      </div>
    </div>`);
  dialog.find('div')
    .prepend($("<br/>"))
    .prepend($("#pageBeanitemMddnxphysicalItemTableitemPolicy_hiddenSelect").clone().attr('id','tu_lAma_itemTemplPolicy').off().removeAttr('onchange').show().removeClass('hide'))
    .prepend($("[for='pageBeanitemMddnxphysicalItemTableitemPolicy']").clone().attr('for','tu_lAma_itemTemplPolicy'));
  dialog.find('div').prepend($("<h3 style='text-align: center;'><span class='de'>Exemplar-Richtlinie Einstellungen</span><span class='en'>Item Policy Settings</span></h3>"));

  $('body').append(dialog);
  dialog.show().find('button#close_settings').click(function(){
    dialog.stop(true).remove();
  });
  dialog.find('button#save_settings').click(function(){
    browser.storage.local.set({
      itemPolicy: dialog.find('#tu_lAma_itemTemplPolicy').val()
    }).then(function(){
      dialog.stop(true).remove();
    });
  });
  dialog.find('button#apply_settings').click(function(){
    browser.storage.local.set({
      itemPolicy: dialog.find('#tu_lAma_itemTemplPolicy').val()
    }).then(function(){
      dialog.stop(true).remove();
      set_item_policy(dialog.find('#tu_lAma_itemTemplPolicy').val());
    });
  });

  browser.storage.local.get().then(function(result){
    if (!!result.itemPolicy) dialog.find('#tu_lAma_itemTemplPolicy').val(result.itemPolicy);
  });
}

function itemTemplateSettings(){
  if (dialog != undefined) dialog.remove();
  dialog = $(`
    <style>
      #itemTemplateSettings .extStatNotes .right, #itemTemplateSettings .extStatNotes.right { margin-left: 250px; }
      #itemTemplateSettings .right { margin-left: 42px; }
      #itemTemplateSettings br { clear: both; }
      #itemTemplateSettings label { margin-bottom: 0; }
      #itemTemplateSettings select, #itemTemplateSettings input { margin-top: 8px; }
      #itemTemplateSettings button { margin-left: 100px; }
      #itemTemplateSettings { position: fixed; bottom: 20px; display: none; width: 100%; z-index: 10000; }
      #itemTemplateSettings span.de, #itemTemplateSettings span.en  { display: none; }
      .extStatNotes, .extIntNotes { display: none; }
      #innerItemTemplateSettings label { clear: both; }
      html[lang='de'] #itemTemplateSettings span.de { display: inline; }
      html[lang='en'] #itemTemplateSettings span.en { display: inline; }
    </style>
    <div id='itemTemplateSettings'>
      <div id='innerItemTemplateSettings' style='width: 900px; margin: auto; background: white; padding: 5px; border: solid #666 1px; border-radius: 4px;'>
        <label><input type='checkbox' id='tu_lAma_itemTemplMoveStatNote' name='tu_lAma_itemTemplMoveStatNote' checked="checked"/></label>
        <span class="control-label height34 displayTable lineHeight35 padSides5 fontReg">
          <span class='de'>Statistiknotizen verschieben bzw. an Notiz 3 anhängen</span>
          <span class='en'>Move statistics notes down and collect in note 3</span>
        </span>
        <label for='tu_lAma_itemTemplStatNote1'>
          <span class='de displayTableCell verticalMiddle'>Statistiknotiz 1</span>
          <span class='en displayTableCell verticalMiddle'>Statistics note 1</span>
        </label>
        <div class="col"><input type='text' id='tu_lAma_itemTemplStatNote1' name='tu_lAma_itemTemplStatNote1' placeholder='Stat Note 1' size='9'/>
          <span class="extStatNotes right">
            prepend <input type="radio" id="tu_lAma_itemTemplStatNote1Beha" name="tu_lAma_itemTemplStatNote1Beha" value="prepend" checked="checked">&nbsp;&nbsp;
            append <input type="radio" id="tu_lAma_itemTemplStatNote1Beha" name="tu_lAma_itemTemplStatNote1Beha" value="append">&nbsp;&nbsp;
            replace <input type="radio" id="tu_lAma_itemTemplStatNote1Beha" name="tu_lAma_itemTemplStatNote1Beha" value="replace">
          </span>
        </div>
        <div class="extStatNotes">
          <label for='tu_lAma_itemTemplStatNote2'>
            <span class='de displayTableCell verticalMiddle'>Statistiknotiz 2</span>
            <span class='en displayTableCell verticalMiddle'>Statistics note 2</span>
          </label>
          <div class="col"><input type='text' id='tu_lAma_itemTemplStatNote2' name='tu_lAma_itemTemplStatNote2' placeholder='Stat Note 2' size='9'/>
            <span class="right">
              prepend <input type="radio" id="tu_lAma_itemTemplStatNote2Beha" name="tu_lAma_itemTemplStatNote2Beha" value="prepend" checked="checked">&nbsp;&nbsp;
              append <input type="radio" id="tu_lAma_itemTemplStatNote2Beha" name="tu_lAma_itemTemplStatNote2Beha" value="append">&nbsp;&nbsp;
              replace <input type="radio" id="tu_lAma_itemTemplStatNote2Beha" name="tu_lAma_itemTemplStatNote2Beha" value="replace">
            </span>
          </div>
          <label for='tu_lAma_itemTemplStatNote3'>
            <span class='de displayTableCell verticalMiddle'>Statistiknotiz 3</span>
            <span class='en displayTableCell verticalMiddle'>Statistics note 3</span>
          </label>
          <div class="col"><input type='text' id='tu_lAma_itemTemplStatNote3' name='tu_lAma_itemTemplStatNote3' placeholder='Stat Note 3' size='9'/>
            <span class="right">
              prepend <input type="radio" id="tu_lAma_itemTemplStatNote3Beha" name="tu_lAma_itemTemplStatNote3Beha" value="prepend" checked="checked">&nbsp;&nbsp;
              append <input type="radio" id="tu_lAma_itemTemplStatNote3Beha" name="tu_lAma_itemTemplStatNote3Beha" value="append">&nbsp;&nbsp;
              replace <input type="radio" id="tu_lAma_itemTemplStatNote3Beha" name="tu_lAma_itemTemplStatNote3Beha" value="replace">
            </span>
          </div>
        </div>
        <br/>
        <label><input type='checkbox' id='tu_lAma_itemTemplMoveIntNote' name='tu_lAma_itemTemplMoveIntNote' checked="checked"/></label>
        <span class="control-label height34 displayTable lineHeight35 padSides5 fontReg">
          <span class='de'>Interne Notizen verschieben bzw. an Notiz 3 anhängen</span>
          <span class='en'>Move internal notes down and collect in note 3</span>
        </span>
        <label for='tu_lAma_itemTemplIntNote1'>
          <span class='de displayTableCell verticalMiddle'>Interne Notiz 1</span>
          <span class='en displayTableCell verticalMiddle'>Internal note 1</span>
        </label>
        <div class="col">
          <input type='text' id='tu_lAma_itemTemplIntNote1' name='tu_lAma_itemTemplIntNote1' placeholder='Internal Note 1 created on {DATE}' size='35'/>
          <span class="extIntNotes right">
            prepend <input type="radio" id="tu_lAma_itemTemplIntNote1Beha" name="tu_lAma_itemTemplIntNote1Beha" value="prepend" checked="checked">&nbsp;&nbsp;
            append <input type="radio" id="tu_lAma_itemTemplIntNote1Beha" name="tu_lAma_itemTemplIntNote1Beha" value="append">&nbsp;&nbsp;
            replace <input type="radio" id="tu_lAma_itemTemplIntNote1Beha" name="tu_lAma_itemTemplIntNote1Beha" value="replace">
          </span>
        </div>
        <div class="extIntNotes">
          <label for='tu_lAma_itemTemplIntNote2'>
            <span class='de displayTableCell verticalMiddle'>Interne Notiz 2</span>
            <span class='en displayTableCell verticalMiddle'>Internal note 2</span>
          </label>
          <div class="col"><input type='text' id='tu_lAma_itemTemplIntNote2' name='tu_lAma_itemTemplIntNote2' placeholder='Internal Note 2 created on {DATE}' size='35'/>
            <span class="right">
              prepend <input type="radio" id="tu_lAma_itemTemplIntNote2Beha" name="tu_lAma_itemTemplIntNote2Beha" value="prepend" checked="checked">&nbsp;&nbsp;
              append <input type="radio" id="tu_lAma_itemTemplIntNote2Beha" name="tu_lAma_itemTemplIntNote2Beha" value="append">&nbsp;&nbsp;
              replace <input type="radio" id="tu_lAma_itemTemplIntNote2Beha" name="tu_lAma_itemTemplIntNote2Beha" value="replace">
            </span>
          </div>
          <label for='tu_lAma_itemTemplIntNote3'>
            <span class='de displayTableCell verticalMiddle'>Interne Notiz 3</span>
            <span class='en displayTableCell verticalMiddle'>Internal note 3</span>
          </label>
          <div class="col"><input type='text' id='tu_lAma_itemTemplIntNote3' name='tu_lAma_itemTemplIntNote3' placeholder='Internal Note 3 created on {DATE}' size='35'/>
            <span class="right">
              prepend <input type="radio" id="tu_lAma_itemTemplIntNote3Beha" name="tu_lAma_itemTemplIntNote3Beha" value="prepend" checked="checked">&nbsp;&nbsp;
              append <input type="radio" id="tu_lAma_itemTemplIntNote3Beha" name="tu_lAma_itemTemplIntNote3Beha" value="append">&nbsp;&nbsp;
              replace <input type="radio" id="tu_lAma_itemTemplIntNote3Beha" name="tu_lAma_itemTemplIntNote3Beha" value="replace">
            </span>
          </div>
        </div>
        <br/>
        <label for='itu_lAma_temTemplSetInvNum'>
          <span class='de displayTableCell verticalMiddle lineHeight16'>Inventarnummer Zuweisen: Sequenzname</span>
          <span class='en displayTableCell verticalMiddle lineHeight16'>Allocate inventory number: Sequence Name</span>
        </label>
        <input type='text' id='tu_lAma_itemTemplSetInvNum' name='tu_lAma_itemTemplSetInvNum' placeholder='inv-alt' size='6'/>
        <br/>
        <label for='tu_lAma_itemTemplSetInvDate'>
          <span class='de displayTableCell verticalMiddle'>Inventardatum setzen</span>
          <span class='en displayTableCell verticalMiddle'>Set inventory date</span>
        </label>
        <input type='checkbox' id='tu_lAma_itemTemplSetInvDate' name='tu_lAma_itemTemplSetInvDate'/>
        <br/>
        <label for='tu_lAma_itemTemplInvPrice'>
          <span class='de displayTableCell verticalMiddle'>Rechnungspreis übernehmen</span>
          <span class='en displayTableCell verticalMiddle'>Take invoice price</span>
        </label>
        <input type='checkbox' id='tu_lAma_itemTemplInvPrice' name='tu_lAma_itemTemplInvPrice'/>
        <br/>
        <label for='tu_lAma_itemTemplGotoNotes'>
          <span class='de displayTableCell verticalMiddle'>Immer zum Notes-Tab wechseln</span>
          <span class='en displayTableCell verticalMiddle'>Go always to notes tab</span>
        </label>
        <input type='checkbox' id='tu_lAma_itemTemplGotoNotes' name='tu_lAma_itemTemplGotoNotes'/>
        <br/>
        <label for='tu_lAma_itemTemplNotNewOnly'>
          <span class='de displayTableCell verticalMiddle lineHeight16'>Nur auf neue Exemplare anwenden</span>
          <span class='en displayTableCell verticalMiddle'>Apply to new items only</span>
        </label>
        <input type='checkbox' id='tu_lAma_itemTemplNotNewOnly' name='tu_lAma_itemTemplNotNewOnly'/>
        <br/>
        <button id='close_settings' style='margin-left: 150px;'>
          <span class='de'>Abbrechen</span>
          <span class='en'>Cancle</span>
        </button>
        <button id='save_settings'>
          <span class='de'>Speichern</span>
          <span class='en'>Save</span>
        </button>
        <button id='apply_settings'>
          <span class='de'>Speichern und Anwenden</span>
          <span class='en'>Save and Apply</span>
        </button>
      </div>
    </div>`);
  dialog.find('label').addClass($("[for='pageBeanpermanentLocationId']").prop('class'));
  dialog.find('div#innerItemTemplateSettings')
    .prepend($("<br/>"))
    .prepend($("#pageBeanpermanentLocationId_hiddenSelect").clone().attr('id','tu_lAma_itemTemplPermLoc').off().removeAttr('onchange').show().removeClass('hide'))
    .prepend($("[for='pageBeanpermanentLocationId']").clone().attr('for','tu_lAma_itemTemplPermLoc'));
  dialog.find('div#innerItemTemplateSettings')
    .prepend($("<br/>"))
    .prepend($("#pageBeanselectedProcessType_hiddenSelect").clone().attr('id','tu_lAma_itemTemplProcType').off().removeAttr('onchange').removeAttr('disabled').show().removeClass('hide'))
    .prepend($("[for='pageBeanselectedProcessType']").clone().attr('for','tu_lAma_itemTemplProcType'));
  dialog.find('div#innerItemTemplateSettings')
    .prepend($("<br/>"))
    .prepend($("#pageBeanitemMddnxphysicalItemTableitemPolicy_hiddenSelect").clone().attr('id','tu_lAma_itemTemplPolicy').off().removeAttr('onchange').show().removeClass('hide'))
    .prepend($("[for='pageBeanitemMddnxphysicalItemTableitemPolicy']").clone().attr('for','tu_lAma_itemTemplPolicy'));
  dialog.find('div#innerItemTemplateSettings')
    .prepend($("<br/>"))
    .prepend($("#pageBeanitemMddnxphysicalItemTablematerialType_hiddenSelect").clone().attr('id','tu_lAma_itemTemplMatType').off().removeAttr('onchange').show().removeClass('hide'))
    .prepend($("[for='pageBeanitemMddnxphysicalItemTablematerialType']").clone().attr('for','tu_lAma_itemTemplMatType'));
  dialog.find('div#innerItemTemplateSettings').prepend($("<h3 style='text-align: center;'><span class='de'>Exemplar Template Einstellungen</span><span class='en'>Item Template Settings</span></h3>"));
  dialog.find("#tu_lAma_itemTemplPermLoc").prepend($("<option value=''>" + browser.i18n.getMessage('unchanged') + "</option>"));
  dialog.find("#tu_lAma_itemTemplPolicy").prepend($("<option value=''>" + browser.i18n.getMessage('unchanged') + "</option>"));
  dialog.find("#tu_lAma_itemTemplProcType").prepend($("<option value='keep'>" + browser.i18n.getMessage('unchanged') + "</option>"));
  dialog.find("#tu_lAma_itemTemplMatType").prepend($("<option value='keep'>" + browser.i18n.getMessage('unchanged') + "</option>"));
  
  $('body').append(dialog);
  dialog.show().find('button#close_settings').click(function(){
    dialog.stop(true).remove();
  });
  dialog.find('button#save_settings').click(function(){
    saveItemTemplateSettings(dialog).then(function(){
      dialog.stop(true).remove();
    });
  });
  dialog.find('button#apply_settings').click(function(){
    saveItemTemplateSettings(dialog).then(function(){
      dialog.stop(true).remove();
      setItemTemplate();
    });
  });
  dialog.find('#tu_lAma_itemTemplMoveStatNote').on('change',function(){
    dialog.find('.extStatNotes').toggle(!this.checked);
  });
  dialog.find('#tu_lAma_itemTemplMoveIntNote').on('change',function(){
    dialog.find('.extIntNotes').toggle(!this.checked);
  });

  browser.storage.local.get().then(function(result){  
    dialog.find('#tu_lAma_itemTemplPolicy').val(!!result.itemTemplPolicy?result.itemTemplPolicy:'');
    dialog.find('#tu_lAma_itemTemplMatType').val(!!result.itemTemplMatType?result.itemTemplMatType:'keep');
    dialog.find('#tu_lAma_itemTemplMoveStatNote').prop("checked",!result.itemTemplMoveStatNote).trigger('change');
    if (!!result.itemTemplStatNote1) dialog.find('#tu_lAma_itemTemplStatNote1').val(result.itemTemplStatNote1);
    if (!!result.itemTemplStatNote1Beha) dialog.find('#tu_lAma_itemTemplStatNote1Beha[value="'+result.itemTemplStatNote1Beha+'"]').prop("checked",true);
    if (!!result.itemTemplStatNote2) dialog.find('#tu_lAma_itemTemplStatNote2').val(result.itemTemplStatNote2);
    if (!!result.itemTemplStatNote2Beha) dialog.find('#tu_lAma_itemTemplStatNote2Beha[value="'+result.itemTemplStatNote2Beha+'"]').prop("checked",true);
    if (!!result.itemTemplStatNote3) dialog.find('#tu_lAma_itemTemplStatNote3').val(result.itemTemplStatNote3);
    if (!!result.itemTemplStatNote3Beha) dialog.find('#tu_lAma_itemTemplStatNote3Beha[value="'+result.itemTemplStatNote3Beha+'"]').prop("checked",true);
    dialog.find('#tu_lAma_itemTemplMoveIntNote').prop("checked",!result.itemTemplMoveIntNote).trigger('change');
    if (!!result.itemTemplIntNote1) dialog.find('#tu_lAma_itemTemplIntNote1').val(result.itemTemplIntNote1);
    if (!!result.itemTemplIntNote1Beha) dialog.find('#tu_lAma_itemTemplIntNote1Beha[value="'+result.itemTemplIntNote1Beha+'"]').prop("checked",true);
    if (!!result.itemTemplIntNote2) dialog.find('#tu_lAma_itemTemplIntNote2').val(result.itemTemplIntNote2);
    if (!!result.itemTemplIntNote2Beha) dialog.find('#tu_lAma_itemTemplIntNote2Beha[value="'+result.itemTemplIntNote2Beha+'"]').prop("checked",true);
    if (!!result.itemTemplIntNote3) dialog.find('#tu_lAma_itemTemplIntNote3').val(result.itemTemplIntNote3);
    if (!!result.itemTemplIntNote3Beha) dialog.find('#tu_lAma_itemTemplIntNote3Beha[value="'+result.itemTemplIntNote3Beha+'"]').prop("checked",true);
    if (!!result.itemTemplProcType && dialog.find(`#tu_lAma_itemTemplProcType option[value='${!!result.itemTemplProcType}']`).length == 0){
      dialog.find("#tu_lAma_itemTemplProcType").prepend($(`<option value='${result.itemTemplProcType}'>${result.itemTemplProcType}</option>`));
    }
    dialog.find('#tu_lAma_itemTemplProcType').val(!!result.itemTemplProcType?result.itemTemplProcType:'keep');
    dialog.find('#tu_lAma_itemTemplSetInvDate').prop("checked",!!result.itemTemplSetInvDate);
    if (!!result.itemTemplSetInvNum) dialog.find('#tu_lAma_itemTemplSetInvNum').val(result.itemTemplSetInvNum);
    dialog.find('#tu_lAma_itemTemplPermLoc').val(!!result.itemTemplPermLoc?result.itemTemplPermLoc:'');
    dialog.find('#tu_lAma_itemTemplNotNewOnly').prop("checked", !result.itemTemplNotNewOnly);
    dialog.find('#tu_lAma_itemTemplInvPrice').prop("checked", !!result.itemTemplInvPrice);
    dialog.find('#tu_lAma_itemTemplGotoNotes').prop("checked", !!result.itemTemplGotoNotes);
  });
}

function saveItemTemplateSettings(dialog){
  return browser.storage.local.set({
    itemTemplPolicy: dialog.find('#tu_lAma_itemTemplPolicy').val(),
    itemTemplMatType: dialog.find('#tu_lAma_itemTemplMatType').val(),
    itemTemplMoveStatNote: !dialog.find('#tu_lAma_itemTemplMoveStatNote').prop("checked"),
    itemTemplStatNote1: dialog.find('#tu_lAma_itemTemplStatNote1').val(),
    itemTemplStatNote1Beha: dialog.find('#tu_lAma_itemTemplStatNote1Beha:checked').val(),
    itemTemplStatNote2: dialog.find('#tu_lAma_itemTemplStatNote2').val(),
    itemTemplStatNote2Beha: dialog.find('#tu_lAma_itemTemplStatNote2Beha:checked').val(),
    itemTemplStatNote3: dialog.find('#tu_lAma_itemTemplStatNote3').val(),
    itemTemplStatNote3Beha: dialog.find('#tu_lAma_itemTemplStatNote3Beha:checked').val(),
    itemTemplMoveIntNote: !dialog.find('#tu_lAma_itemTemplMoveIntNote').prop("checked"),
    itemTemplIntNote1: dialog.find('#tu_lAma_itemTemplIntNote1').val(),
    itemTemplIntNote1Beha: dialog.find('#tu_lAma_itemTemplIntNote1Beha:checked').val(),
    itemTemplIntNote2: dialog.find('#tu_lAma_itemTemplIntNote2').val(),
    itemTemplIntNote2Beha: dialog.find('#tu_lAma_itemTemplIntNote2Beha:checked').val(),
    itemTemplIntNote3: dialog.find('#tu_lAma_itemTemplIntNote3').val(),
    itemTemplIntNote3Beha: dialog.find('#tu_lAma_itemTemplIntNote3Beha:checked').val(),
    itemTemplProcType: dialog.find('#tu_lAma_itemTemplProcType').val(),
    itemTemplSetInvDate: dialog.find('#tu_lAma_itemTemplSetInvDate').prop("checked"),
    itemTemplSetInvNum: dialog.find('#tu_lAma_itemTemplSetInvNum').val(),
    itemTemplPermLoc: dialog.find('#tu_lAma_itemTemplPermLoc').val(),
    itemTemplNotNewOnly: !dialog.find('#tu_lAma_itemTemplNotNewOnly').prop("checked"),
    itemTemplInvPrice: dialog.find('#tu_lAma_itemTemplInvPrice').prop("checked"),
    itemTemplGotoNotes: dialog.find('#tu_lAma_itemTemplGotoNotes').prop("checked")
  });
}

function setItemTemplate(){
  debug("setItemTemplate").then(function(m){console.log(m)});
  debug(this,2).then(function(m){console.log(m)});
  var ST = {
    Start: 1,
    Material: 2,
    OpenInventoryNumber: 3,
    AddInventoryNumber: 4,
    SaveAndEdit: 5,
    ProcType: 6,
    ItemPol: 7,
    InvPrice: 8,
    GotoNotes: 9,
    AllDone: 255,
  }

  var state = ST.Start;
  var msg = '';
  var col = '#f8fff8';
  var obstimer;
  var date = createDate();
  var observer;


  function createDate(){
    var d = new Date();
    var y = d.getFullYear();
    var m = d.getMonth()+1;
    var t = d.getDate();
    if (m<10) m='0' + m;
    if (t<10) t='0' + t;
    return t+'.'+m+'.'+y;
    $('#pageBeandataObjectinventoryDate').val(t+'.'+m+'.'+y);
  }

  function openInveroryNumber(){
    debug(state,3).then(function(m){console.log(m)});
    state = ST.OpenInventoryNumber;
    $('#cresource_editorpitem_generalallocate').click();
  }

  function clickInveroryNumber(label){
    debug(state,3).then(function(m){console.log(m)});
    state = ST.AddInventoryNumber;
    $('#sequenceRows span.labelField').filter(function(){
      return $(this).text() == label;
    }).closest('tr').find('input').click();
    $('#PAGE_BUTTONS_cresource_editorpitem_generalpopupselect').click();
  }

  function saveAndEdit(result){
    // let newOnly = (!result.itemTemplNotNewOnly || (!result.itemTemplStatNote1 && !result.itemTemplIntNote1));
    debug(state,3).then(function(m){console.log(m)});
    state = ST.SaveAndEdit;
    if ($('#PAGE_BUTTONS_cresource_editorpitem_generalsave_and_edit').length == 1)
      $('#PAGE_BUTTONS_cresource_editorpitem_generalsave_and_edit').click();
    else if (!!result.itemTemplNotNewOnly && result.itemTemplProcType != undefined && result.itemTemplProcType != 'keep')
      protype(result.itemTemplProcType);
    else if (!!result.itemTemplNotNewOnly)
      invPrice(result);
    else
      allDone();
  }

  function protype(pt){
    debug(state,3).then(function(m){console.log(m)});
    state = ST.ProcType;
    debug('change process type to ' + pt,2).then(function(m){console.log(m)});
    var ptype = $("#pageBeanselectedProcessType_hiddenSelect option[value='"+pt+"']");
    ptype.prop('selected',true);
    $("#pageBeanselectedProcessType_hiddenSelect").get(0).onchange();
  }

  function invPrice(result){
    debug(state,3).then(function(m){console.log(m)});
    state = ST.InvPrice;
    let poline = $('#pageBeanpoLineRef').val();
    if (poline != '' && !!result.itemTemplInvPrice && !!result.apiKey && !!result.apiURL){
      $.ajax({
        url:      result.apiURL + '/almaws/v1/acq/po-lines/' + poline,
        type:     'GET',
        headers:  { 'Authorization': 'apikey ' + result.apiKey },
        data:     { format: 'json' },
        success:  function(data){
          if (!data.po_number){
            col = '#ff8f8f';
            msg += 'PO_Number nicht gefunden! ';
            gotonotes(result);
          } else {
            $.ajax({
              url:      result.apiURL + '/almaws/v1/acq/invoices?q=po_number~' + data.po_number,
              type:     'GET',
              headers:  { 'Authorization': 'apikey ' + result.apiKey },
              success:  function(data){
                let price = parseFloat($(data).find('invoice_lines:has(po_line:contains("'+ poline +'")) total_price').text());
                $('input#pageBeanitemMddnxphysicalItemTableinventoryPrice').val(price.toFixed(2).toString().replace('.',','));
                col = '#8fff8f';
                msg += 'Preis aus der Rechnung: ' + price;
                gotonotes(result);
              },
              error: function(){
                col = '#ff8f8f';
                msg += 'Rechnung(-szeile) nicht gefunden! ';
                gotonotes(result);
              }
            });
          }
        },
        error: function(){
          gotonotes(result);
        }
      });
    } else {
      // col = '#ff8f8f';
      // msg += 'Keine Bestellposten vorhanden!';
      gotonotes(result);
    }
  }

  function gotonotes(result){
    debug(state,3).then(function(m){console.log(m)});
    if (!!result.itemTemplStatNote1 || !!result.itemTemplIntNote1 || !!result.itemTemplGotoNotes){
      state = ST.GotoNotes;
      $("#cresource_editornotes_span").click();
    } else {
      allDone();
    }
  }

  function addStatNote(result){
    var note = new Array();
    note[1] = $('#pageBeanitemMddnxphysicalItemTablestatisticsNote_1');
    note[2] = $('#pageBeanitemMddnxphysicalItemTablestatisticsNote_2');
    note[3] = $('#pageBeanitemMddnxphysicalItemTablestatisticsNote_3');
    if (note[1].length == 0 || note[2].length == 0 || note[3].length == 0){
      col = '#ff8f8f';
      msg += 'Statistiknotiz nicht gefunden! ';
    } else {
      if (!result.itemTemplMoveStatNote){
        if (!!result.itemTemplStatNote1){
          if (note[3].val().trim() != '' && note[3].val().trim() != ';'){
            note[3].val(note[2].val() + '; ' + note[3].val());
          } else {
            note[3].val(note[2].val());
          }
          note[2].val(note[1].val());
          note[1].val(result.itemTemplStatNote1.replace('{DATE}',date));
        }
      } else {
        for (i = 1; i < 4; i++){
          let setId = 'itemTemplStatNote'+i;
          if (!!result[setId+'Beha'] && !!result[setId]){
            if (result[setId+'Beha'] == 'replace' || note[i].val().trim() == '') note[i].val(result[setId]);
            else if (result[setId+'Beha'] == 'prepend') note[i].val((result[setId]+' '+note[i].val()).trim());
            else if (result[setId+'Beha'] == 'append') note[i].val((note[i].val()+' '+result[setId]).trim());
          }
        }
      }
    }
  }

  function addIntNote(result){
    var note = new Array();
    note[1] = $("#pageBeanitemMddnxphysicalItemTableinternalNote_1");
    note[2] = $("#pageBeanitemMddnxphysicalItemTableinternalNote_2");
    note[3] = $("#pageBeanitemMddnxphysicalItemTableinternalNote_3");
    if (note[1].length == 0 || note[2].length == 0 || note[3].length == 0){
      col = '#ff8f8f';
      msg += 'Notiz nicht gefunden! ';
    } else {
      if (!result.itemTemplMoveIntNote){
        if (!!result.itemTemplIntNote1){
          if (note[3].val().trim() != '' && note[3].val().trim() != ';'){
            note[3].val(note[2].val() + '; ' + note[3].val());
          } else {
            note[3].val(note[2].val());
          }
          note[2].val(note[1].val());
          note[1].val(result.itemTemplIntNote1.replace('{DATE}',date));
        }
      } else {
        for (i = 1; i < 4; i++){
          let setId = 'itemTemplIntNote'+i;
          if (!!result[setId+'Beha'] && !!result[setId]){
            if (result[setId+'Beha'] == 'replace' || note[i].val().trim() == '') note[i].val(result[setId]);
            else if (result[setId+'Beha'] == 'prepend') note[i].val((result[setId]+'; '+note[i].val()).trim());
            else if (result[setId+'Beha'] == 'append') note[i].val((note[i].val()+'; '+result[setId]).trim());
          }
        }
      }
    }
  }

  function allDone(){
    debug(state,2).then(function(m){console.log(m)});
    clearTimeout(obstimer);
    state = ST.AllDone;
    observer.disconnect();
    browser.runtime.sendMessage({
      action: "toast",
      message: msg,
      color: col,
      duration: 10
    });
    window.tulama_busy = false;
  }

  browser.storage.local.get().then(function(result){
    if ($('#PAGE_BUTTONS_cresource_editorpitem_generalsave_and_edit').length == 0 && !result.itemTemplNotNewOnly){
      browser.runtime.sendMessage({
        action: "toast",
        message: "laut Einstellungen nur bei neuen Exemplaren",
        color: '#ff8f8f',
        duration: 20
      });
      return;
    }

    observer = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {
        // console.log(state);
        debug(mutation,4).then(function(m){console.log(m)});
        if (mutation.attributeName = "class" && $(mutation.target).hasClass("hide")){
          switch (state){
            case ST.OpenInventoryNumber:
              clickInveroryNumber(result.itemTemplSetInvNum);
              break;
            case ST.AddInventoryNumber:
              // if (!!result.itemTemplNotNewOnly && result.itemTemplProcType != undefined && result.itemTemplProcType != 'keep')
              //  protype(result.itemTemplProcType);
              // else
                saveAndEdit(result);
              break;
            case ST.SaveAndEdit:
              if (result.itemTemplProcType != undefined && result.itemTemplProcType != 'keep')
                protype(result.itemTemplProcType);
              else
                invPrice(result);
              break;
            case ST.ProcType:
              invPrice(result);
              break;
            case ST.GotoNotes:
              addStatNote(result);
              addIntNote(result);
              $("#pageBeanitemMddnxphysicalItemTableinternalNote_1").focus();
              allDone();
          }
        }
      });
    });

    if (!!result.itemTemplPolicy){
      $("#pageBeanitemMddnxphysicalItemTableitemPolicy_hiddenSelect option[value='"+ result.itemTemplPolicy +"']").prop('selected',true).trigger('change');
      $("#pageBeanitemMddnxphysicalItemTableitemPolicy").val($("#pageBeanitemMddnxphysicalItemTableitemPolicy_hiddenSelect option:selected").text());
    }
    if (result.itemTemplMatType != undefined && result.itemTemplMatType != 'keep'){
      $("#pageBeanitemMddnxphysicalItemTablematerialType_hiddenSelect option[value='"+ result.itemTemplMatType +"']").prop('selected',true).trigger('change');
      $("#pageBeanitemMddnxphysicalItemTablematerialType").val($("#pageBeanitemMddnxphysicalItemTablematerialType_hiddenSelect option:selected").text());
    }
    if (!!result.itemTemplSetInvDate){
      $('#pageBeandataObjectinventoryDate').val(date);
    }
    window.tulama_busy = true;
    observer.observe($('#loadingBlocker').get(0),{childList:true,subtree:true,attributes:true});
    obstimer = setTimeout(function(){
      debug(state + ' main observer timed out!').then(function(m){console.log(m)});
      msg += 'Main observer timed out! ';
      col = '#ff8f8f';
      allDone();
    },30000);
    if (!!result.itemTemplSetInvNum){
      openInveroryNumber();
//    } else if (!!result.itemTemplNotNewOnly && result.itemTemplProcType != undefined && result.itemTemplProcType != 'keep'){
//      protype(result.itemTemplProcType);
    } else {
      saveAndEdit(result);
    }
  });
}

function expandSearchValues(dummy){
  if ($('#advancedSearchWrapper:hidden').length == 1){
    $('#advancedLink').get(0).dispatchEvent(click);
  } else {
    var list = $('.advSearchValueWrapper input').val().split(' ');
    list = list.filter(function(val){ return val.trim() != ''; });
    if (list.length > 100 && !confirm('Wirklich ' + list.length + ' Werte expandieren?')) return;
    debug(list,2).then(function(m){console.log(m)});
    list.forEach(function(val, i){
      $('.advSearchValueWrapper input').last().val(val);
      if (list.length > i+1){
        $('.duplicateRowBtn').last().get(0).dispatchEvent(click);
        $('.advAndOrWrapper input[value="OR"]').last().click().get(0).dispatchEvent(click);
      }
    });
    setTimeout(function(){ $('#advSearchBtn').get(0).scrollIntoView(false); window.scrollBy(0, 100); },500);
    browser.runtime.sendMessage({
      action: "toast",
      message: list.length + ' Werte!',
      color: '#8fff8f',
      duration: 20
    });
  }
}

function add_loans(what){
  browser.storage.local.get().then(function(result){
    if ((isTuw && !!result.loansUrl) || document.URL.includes('ubtuw')){
      if (!result.loansUrl) result.loansUrl = 'https://almagw.ub.tuwien.ac.at/tulama/loans.php';
      if (what == 'itemlist'){
        var physiids = $("input[name='pageBean.selectedPids'][type='checkbox']").map(function() { return this.value; }).get();
        $.post(result.loansUrl,
          {
            'ITEMID[]': physiids
          },
          function(data){
            $('#SELENIUM_ID_list_HEADER_accessionNumber').text('Entl./heuer/letzte');
            data.forEach(function(val){
              $("#recordContainerlist" + val.item_id + " span[id$='COL_accessionNumber']").text(val.loans + '/' + val.loans_this_year + '/' + val.last_loan);
            });
          },"json").fail(function(jqXHR, textStatus, errorThrown) {
            debug(textStatus,2).then(function(m){console.log(m)});
          }
        );
      } else if (what == 'holdinglist'){
        var holids = $("input[name='pageBean.selectedPids'][type='checkbox']").map(function() { return this.value; }).get();
        $.post(result.loansUrl,
          {
            'HOLID[]': holids
          },
          function(data){
            $('#SELENIUM_ID_listWithFilters_HEADER_accessionNumber input[type="submit"]').val('Entl./heuer/letzte');
            data.forEach(function(val){
              $("#recordContainerlistWithFilters" + val.hol_id + " span[id$='COL_accessionNumber']").text(val.loans + '/' + val.loans_this_year + '/' + val.last_loan );
            });
          },"json").fail(function(jqXHR, textStatus, errorThrown) {
            debug(textStatus,2).then(function(m){console.log(m)});
          }
        );
      }
    }
  });
}

function handleKeyEvent(e){
  var oe = e.originalEvent;
  debug(e,2).then(function(m){console.log(m)});
  if ((oe.altKey || oe.metaKey) && oe.ctrlKey && e.data[oe.key] != undefined)
    e.data[oe.key].action(e.data[oe.key].value);
}

function enable_key_shortcuts(){
  $(document).unbind('keydown.editPhysicalItem');
  $(document).unbind('keydown.PhysicalItemList');
  $(document).unbind('keydown.expandSearchValues');
  var kdevent = false;
  var keys = {
    y:{ value: 'expandSearchValues',
        action: expandSearchValues
      },
    L:{ value:  1,
        action: lamaTwinkle
      }
  };
  if ($('body').attr('id') == 'body_id_xml_file_resource_editor.physical.item_general.xml'){
    kdevent = 'keydown.editPhysicalItem';
    keys.b = { value: 'GG', action: protype };
    keys.B = { value: 'dummy', action: proTypeSettings };
    keys.c = { value: 'Null', action: set_item_policy };
    keys.C = { value: 'Null', action: itemPolicySettings };
    keys.t = { value: 'dummy', action: setItemTemplate };
    keys.T = { value: 'dummy', action: itemTemplateSettings };
    debug('add keydown event_handler for "edit physical item"',2).then(function(m){console.log(m)});
  } else if ($('body').attr('id') == 'body_id_xml_file_resource_editor.physical.items_list.xml'){
    kdevent = 'keydown.physicalItemList';
    keys.d = { value: 'Description', action: sort_items };
    if (isTuw){
      keys.l = { value: 'itemlist', action: add_loans };
      keys.L = { value: 'itemlist', action: add_loans };
    }
    debug('inst strg-alt-d + l',2).then(function(m){console.log(m)});
  } else if ($('body').attr('id') == 'body_id_xml_file_resource_editor.physical.holdings_list.xml'){
    kdevent = 'keydown.physicalHoldingList';
    if (isTuw){
      keys.l = { value: 'holdinglist', action: add_loans };
      keys.L = { value: 'holdinglist', action: add_loans };
    }
    debug('inst strg-alt-d + l',2).then(function(m){console.log(m)});
  } else if ($('#persistentSearchWrapper').length == 1){
    kdevent = 'keydown.expandSearchValues';
    debug('add keydown event_handler for "expandSearchValues"',2).then(function(m){console.log(m)});
  }
  debug(kdevent,2).then(function(m){console.log(m)});
  debug(keys,2).then(function(m){console.log(m)});
  if (!!kdevent){
    $(document).bind(
      kdevent,
      keys,
      function(e) {
        handleKeyEvent(e);
      }
    );
  }
}

function getLocalIPs(callback) {
    var ips = [];

    var RTCPeerConnection = window.RTCPeerConnection ||
        window.webkitRTCPeerConnection || window.mozRTCPeerConnection;

    var pc = new RTCPeerConnection({
        // Don't specify any stun/turn servers, otherwise you will
        // also find your public IP addresses.
        iceServers: []
    });
    // Add a media line, this is needed to activate candidate gathering.
    pc.createDataChannel('');

    // onicecandidate is triggered whenever a candidate has been found.
    pc.onicecandidate = function(e) {
        if (!e.candidate) { // Candidate gathering completed.
            pc.close();
            callback(ips);
            return;
        }
        var ip = /^candidate:.+ (\S+) \d+ typ/.exec(e.candidate.candidate)[1];
        if (ips.indexOf(ip) == -1) // avoid duplicate entries (tcp/udp)
            ips.push(ip);
    };
    pc.createOffer(function(sdp) {
        pc.setLocalDescription(sdp);
    }, function onerror() {});
}

function chooseLocation(result){
  if (document.URL.search('login.choose_unit.xml') !== -1) {
    var locationSelect = $('#pageBeanselectedLocation_hiddenSelect');
    if (locationSelect.length > 0){
      if (!!result.selectedLocation){
        debug("select location: " + result.selectedLocation[0] + ' ' + result.selectedLocation[1]).then(function(m){console.log(m)});
        $("#pageBeanselectedLocation_hiddenSelect option[value='"+ result.selectedLocation[0] +"']").prop('selected',true).trigger("change");
        $("#pageBeanselectedLocation").val($("#pageBeanselectedLocation_hiddenSelect option[value='" + result.selectedLocation + "']").text())
        $("#PAGE_BUTTONS_popupselect").trigger('click');
      } else {
        $('#PAGE_BUTTONS_popupselect').on('click',function(){
          debug("save location: " + this.value).then(function(m){console.log(m)});
          browser.storage.local.set({ selectedLocation: locationSelect.val() });
        });
      }
    }
  } else {
    if ((typeof result.selectedLocation) == 'object' && result.selectedLocation[1] != undefined && result.selectedLocation[0].match(/Null|\d{10,}/)
       && $('#ALMA_MENU_TOP_NAV_location button #locationText').length > 0
       && $('#ALMA_MENU_TOP_NAV_location button #locationText').text() != result.selectedLocation[1]){
      debug("location: " + $('#ALMA_MENU_TOP_NAV_location button #locationText').text()).then(function(m){console.log(m)});
      debug(result.selectedLocation).then(function(m){console.log(m)});
      $('#MENU_LOCATION_LIST').attr('onchange',$('#MENU_LOCATION_LIST').attr('onchange').replace("$('#MENU_LOCATION_LIST_hiddenSelect').val()","'"+result.selectedLocation[0]+"'"));
      debug($('#MENU_LOCATION_LIST').attr('onchange')).then(function(m){console.log(m)});
      if ($('#MENU_LOCATION_LIST').attr('onchange').search(result.selectedLocation[0]) !== -1)
        $('#MENU_LOCATION_LIST').get(0).onchange();
    }
  }
}

function addColorTemplate(){
  if ($('#TABLE_DATA_marcFieldsList tr:has(span[title="970"]) td[id$="_COL_value"]').filter(function(){
    return (this.textContent.match(/Dublette|löschen|gelöscht/i) != null)
  }).length > 0){
    $('body').addClass('tu_lAma_Dublette');
    $('.upperActionsLeft .tableSearch').append($('<span class="tu_lAma_Dublette"><h2>Dublette bzw. zum Löschen vorgesehen</h2></span>'));
  } else {
    $('body').removeClass('tu_lAma_Dublette');
  }
  browser.storage.local.get().then(function(result){
    if (result.colorScheme == undefined) result.colorScheme = 'rgb';
    if (!!result.colorScheme){
      var re = /^(..)? (\|. )$/;
      $("td[id$='_COL_value']").contents().each(function(i,n){
        if (n.nodeName=='#text' && (match=re.exec(n.data))){
          // debug(match).then(function(m){console.log(m)});
          if (match[1] == undefined)
            $(n).replaceWith('<span class="subField">' + n.data + '</span>');
          else
            $(n).replaceWith('<span class="indicator">' + match[1] + '</span> <span class="subField">' + match[2] + '</span>');
        }
        // debug(n).then(function(m){console.log(m)});
      });
    }
  });
}

function addMDEColors(){
  if (!!document.getElementById('tu_lAma_blocker')) return;
  var dubl = $('#MarcEditorPresenter\\.textArea\\.970 textarea').filter(function(){
    return (this.value.match(/Dublette|löschen/i) != null)
  });
  $('.marcEditor.mainContainer').removeClass('tu_lAma_Dublette');
  if (dubl.length > 0){
    dubl.closest('.marcEditor.mainContainer').addClass('tu_lAma_Dublette');
    toast("<h3 style='color: black;'>Dublette bzw. zum Löschen vorgesehen!</h3>", "#E00009", 10);
  }
  if ($('.alenTagColor').length > 0){
    toast("<h2 style='color: black;'>Bitte das Add-On 'AlEn' deaktivieren bzw. entfernen</h2><h3 style='margin-bottom: 30px; margin-top: 10px; color: black;'>Es ist mit manchen Bookmarklets inkompatibel und unser lAma hat nun eigene lustige Farben</h3>", "orange", 20);
  }
  browser.storage.local.get().then(function(result){
    if (result.colorScheme == undefined) result.colorScheme = 'rgb';
    if (!!result.colorScheme){
      $('[id^="MarcEditorPresenter\\.textArea"] div:has("#titleFieldLabelId")').show().on('mousedown',function(event){
        // prevent text selection in view mode, delegate mousedown to textarea? 
        $(event.target).click();
        /* var mousedown = new event.constructor(event.type, event.originalEvent);
        console.log(mousedown);
        try { $(this).parent().find('div:has("textarea")').find("textarea").get(0); }
        catch(e){ colsole.log(e);}
        console.log(mousedown);
        event.preventDefault();
        event.stopPropagation(); */
      }).on("click focus",function(){
        debug("clicked or focus",2).then(function(m){console.log(m)});
        // console.log(window.getSelection());
        // console.log(window.getSelection().toString());
        $(this).parent().find('div:has("textarea")').find("textarea").focus();
      }).find("#titleFieldLabelId").contents().each(function(i,n){
        // debug(n).then(function(m){console.log(m)});
        // debug($(n).html()).then(function(m){console.log(m)});
        if (n.nodeName=='#text')
          $(n).replaceWith($('<div/>').text(n.data).html().replace(/(\$\$.) /g,'<span class="subField">$1</span> '));
      });
      $('[id^="MarcEditorPresenter\\.textArea"] div:has("textarea")').find('textarea').on("focus click change",function(){
        debug("focus click change",2).then(function(m){console.log(m)});
        // debug(this).then(function(m){console.log(m)});
        // $(this).parent().parent().find('div:has("#titleFieldLabelId")');
      }).on("blur",function(){
        debug("blur",2).then(function(m){console.log(m)});
        debug(this,3).then(function(m){console.log(m)});
        $(this).parent().parent().find('div:has("#titleFieldLabelId")').find("#titleFieldLabelId").text(this.value).contents().each(function(i,n){
          if (n.nodeName=='#text')
            $(n).replaceWith($('<div/>').text(n.data).html().replace(/(\$\$.) /g,'<span class="subField">$1</span> '));
        });
      });
    }
  });
}

function lamaTwinkle(active = false){
  debug(active,3).then(function(m){console.log(m)});
  if (active == 1){
    $('body').toggleClass('tu_lAma_lb');
    browser.storage.local.set({ lamaLoadingBlocker: $('body').hasClass('tu_lAma_lb') });

    var actoffs = $(document.activeElement).offset();
    if (actoffs.top == 0 || actoffs.left == 0){
      actoffs.top = Math.random() * ($( window ).height() - 48 ) + 48;
      actoffs.left = Math.random() * ($( window ).width() - 300 ) + 48;
    }
    var css = { top: $('#tu_lAma_logo').css('top'), left: $('#tu_lAma_logo').css('left'), display: "none", opacity: 1 };
    $('#tu_lAma_logo').css(actoffs).css("opacity", 1).fadeIn(2000).delay(1000).animate({left: "+=200", opacity: 0 },2500,'linear', function(){$('#tu_lAma_logo').css(css)});
  } else {
    var col = { "border-right-color": $('.backToHomePageBorder').css('border-right-color') };
    $('.backToHomePageBorder').css('border-right-color', "white");
    $('#tu_lAma_logo').delay(1000).fadeIn(2000).delay(1000).fadeOut(2000,function(){$('.backToHomePageBorder').css(col);});
  }
}

function showLaufzettel(e){
  e.preventDefault();
  let barcode = $(this).closest('tr, div.rowsContainer').find('input[id$="barcode"]').val();
  var popupURL = browser.extension.getURL('templates/processSlip.html?barcode='+encodeURIComponent(barcode));
  browser.runtime.sendMessage({
    action: "openPopup",
    popupURL: popupURL
  });
  return false;
}

function laufzettel(){
  browser.storage.local.get().then(function(result){
    if (!!result.processSlip && !!result.apiURL && !!result.apiKey){
      if ($('body').attr('id') == 'body_id_xml_file_loan.fulfillment_patron_workspace_loan_new_ui.xml'
       || $('body').attr('id') == 'body_id_xml_file_resource_editor.physical.items_list.xml'
       || $('body').attr('id') == 'body_id_xml_file_po.poline_receiving_items_list.xml')
        $('td.listActionContainer ul:not(:has(li#processSlip))').append($('<li id="processSlip" class="clearfix"><a class="submitUrl" href="#" title="'+browser.i18n.getMessage('processSlip')+'">'+browser.i18n.getMessage('processSlip')+'</a></li>').on('click',showLaufzettel));
      if ($('body').attr('id') == 'body_id_xml_file_search.physical_item_results.xml')
        $('div.rowActionsContainer ul.internalRowActions:not(:has(li#processSlip))').append($('<li id="processSlip" class="rowAction internalRowAction"><a class="submitUrl" href="#" title="'+browser.i18n.getMessage('processSlip')+'">'+browser.i18n.getMessage('processSlip')+'</a></li>').on('click',showLaufzettel));
    }
  });
}

var initialized;

$(function(){

  if (document.URL.search('alma.exlibrisgroup.com/view/uresolver') === -1){
    debug(initialized,1).then(function(m){console.log(m)});
    if (initialized != undefined) initialized = true;
    // debug($('html').attr('lang')).then(function(m){console.log(m)});
    // browser.i18n.LanguageCode = $('html').attr('lang');
    // debug(browser.i18n.LanguageCode).then(function(m){console.log(m)});
    // auto choose location
    if (!!document.URL.includes('obv-at-ubtuw')){
      isTuw = true;
      browser.storage.local.set({ tuwSeen: true });
    }

    browser.storage.local.get().then(function(result){
      debug(result,3).then(function(m){console.log(m)});
      if (!result.selectLocation){
        if (!!result.locMap){
          getLocalIPs(function(ips){
            var mapped = false;
            ips.forEach(function(ip){
              if (result.locMap[ip] != undefined){
                result.selectedLocation = result.locMap[ip];
                chooseLocation(result);
                mapped = true;
              }
            });
            if (!mapped){
              chooseLocation(result);
            }
          });
        } else {
          chooseLocation(result);
        }
        if (!!(match = document.URL.match(/pageBean\.selectedLocation=(\d*|Null)\&/))){
          debug("update location: " + match[1] + ': ' + $('#ALMA_MENU_TOP_NAV_location #locationText').text(),2).then(function(m){console.log(m)});
          browser.storage.local.set({ selectedLocation: [match[1],$('#ALMA_MENU_TOP_NAV_location #locationText').text()]});
        }
      }
      if (result.colorScheme == undefined) result.colorScheme = 'rgb';
      if (!!result.colorScheme){
        debug('add class ' + result.colorScheme + ' to body',2).then(function(m){console.log(m)});
        $('body').addClass('tu_lAma_marc_' + result.colorScheme);
        if (!document.URL.includes('psb.alma') && !document.URL.includes('sandbox') && (!!result.overwriteAlmaColors))
          $('body').addClass('tu_lAma_bg_' + result.colorScheme);
      }
      if (!!result.highContrast)
        $('body').addClass('tu_lAma_marc_hc');
      if (!!result.lamaLoadingBlocker)
        $('body').addClass('tu_lAma_lb');
    });

    if ($('title').text() == 'Md Editor'){
      debug('mdeditor',2).then(function(m){console.log(m)});
      var state = 0;
      var observer = new MutationObserver(function(mutations) {
        if (!!document.getElementById('tu_lAma_blocker')) return;
        mutations.forEach(function(mutation) {
          if (mutation.removedNodes.length >= 1){
            if (state == 0 && mutation.removedNodes[0].id == "loadingWrapper"){
              state = 1;
              debug('loadingWrapper removed',2).then(function(m){console.log(m)});
            } else if (state == 1 && $(mutation.removedNodes).find('#MdEditor\\.PopupMsg\\.LOADING').length >= 1){
              debug('#MdEditor.PopupMsg.LOADING removed',2).then(function(m){console.log(m)});
              if ($('table.MenuIconBarUxp').length == 0){
                setTimeout(addButtons,1000);
              } else {
                addButtons();
              }
              state = 255;
              clearTimeout(obstimer);
              observer.disconnect();
              addMDEColors();
              gotoFirstEmpty();
              catlevel();
              tuwsys();
              checkbug00576546();
            }
            debug({'rm': mutation.removedNodes[0]},4).then(function(m){console.log(m)});
          }
          if (mutation.addedNodes.length >= 1){
            debug({'ad': mutation.addedNodes[0]}, 4).then(function(m){console.log(m)});
          }
        });
      });
      observer.observe($('body').get(0),{childList:true,subtree:false});
      var obstimer = setTimeout(function(){
        debug('main observer timed out',2).then(function(m){console.log(m)});
        observer.disconnect();
      },60000);
    } else if (document.URL.indexOf('/rep/MDEditor.jsp') === -1){
      $('#almaHeader').append($('<img id="tu_lAma_logo" src="' + browser.extension.getURL("icons/tulama-48.gif") + '" style="position: absolute; left: 100px; height: 32px; top: 8px; display: none; "/>'));
      $('#loadingBlocker div.activeLoader').append($('<img id="tu_lAma_lb" src="' + browser.extension.getURL("icons/tulama-64.gif") + '"/>'));
      if (document.title == 'Ex Libris - Alma'
       && document.URL.indexOf('alma.exlibrisgroup.com/mng/action/home.do') === -1
       && document.referrer.indexOf('xmlFileName=configuration_setup.configuration_mngUXP.xml') === -1) setTimeout(lamaTwinkle, 2000);
      debug('outside ' + $('body').attr('id') + ' url: ' + document.URL, 2).then(function(m){console.log(m)});
      browser.runtime.onMessage.addListener((request) => {
        debug("Message from the background script:",2).then(function(m){console.log(m)});
        debug(request,2).then(function(m){console.log(m)});
        if (request.action == 'search'){
          setSimpleSearchKey(request.what);
          $('#ALMA_MENU_TOP_NAV_Search_Text').val(request.id).trigger('change');
          $('button#simpleSearchBtn').trigger('click');
        } else if (request.action == 'historyStateUpdated'){
          // var active = $('.recordListContainer li.jsFocusLineInList');
          // if (active.length > 0)
          //  active.get(0).scrollIntoView(false);
          
          debug($('body').attr('id'),3).then(function(m){console.log(m)});
          /* if ($('body').is('[id*="record_simple_view"]')){
            // addRecordViewLinks();
            // addColorTemplate();
          } else {
            $('body').removeClass('tu_lAma_Dublette');
          } */   
          enable_key_shortcuts();
          // laufzettel();
        } else if (request.action == 'toast'){
          toast(request.message, request.color, request.duration);
        }
      });
      enable_key_shortcuts();

      $('#MENU_LOCATION_LIST_hiddenSelect, #MENU_LOCATION_LIST').on('change',function(){
        debug(this.value,2).then(function(m){console.log(m)});
      });
      $('html').on('hashchange',function(e){
        debug(this,3).then(function(m){console.log(m)});
        debug(e,3).then(function(m){console.log(m)});
      });

      var lbsiobserver = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
          if (mutation.target.className == 'hidden'){
            laufzettel();
            if ($('body').is('[id*="record_simple_view"]')){
              addRecordViewLinks();
              addColorTemplate();
            } else {
              $('body').removeClass('tu_lAma_Dublette');
            }
          }
        });
      });
      lbsiobserver.observe($('#loadingBlockerStatusIdentifier').get(0),{childList:false,subtree:false,attributes:true});
    }

    console.log('TU lAma loaded');
  } else {
    debug('primo iframe ignored',1).then(function(m){console.log(m)});
  }
});
